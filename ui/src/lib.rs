/* ui/src/main.rs
 *
 * Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

mod component;
mod js;
mod page;
mod store;
mod util;

use std::{collections::HashMap, ops::Deref};

use component::{Header, Message, MessageType, PageMessage};
use page::{switch, Route};
use roi::db::{Account, AccountId, Portfolio, Transaction};
use serde::Deserialize;
use serde_wasm_bindgen::from_value;
use wasm_bindgen::{prelude::Closure, JsValue};
use yew::{function_component, html, platform::spawn_local, use_effect, Html};
use yew_router::{BrowserRouter, Switch};
use yewdux::prelude::use_store;

use crate::store::{State, WindowSize};

#[function_component]
pub fn App() -> Html {
    let (state, dispatch) = use_store::<State>();

    {
        let state = state.clone();

        use_effect(move || {
            if let State::Loading = *state {
                spawn_local(async move {
                    // {{{ Handle new data
                    {
                        let dispatch = dispatch.clone();

                        let handle_new_data = Closure::wrap(Box::new(move |event: JsValue| {
                            #[derive(Debug, Deserialize)]
                            struct Event {
                                payload: (
                                    Vec<Portfolio>,
                                    Vec<Account>,
                                    HashMap<AccountId, Transaction>,
                                ),
                            }

                            match from_value(event) {
                                Err(err) => dispatch.set(State::LoadingFailed(err.to_string())),
                                Ok(Event { payload }) => dispatch.reduce(|state| {
                                    state
                                        .as_ready()
                                        .map(|data| store::Data {
                                            portfolios: payload.0.clone(),
                                            accounts: payload.1.clone(),
                                            transactions: payload.2.clone(),
                                            ..data.clone()
                                        })
                                        .into()
                                }),
                            }
                        })
                            as Box<dyn FnMut(JsValue)>);

                        js::listen("refresh_data", handle_new_data.into_js_value()).await;
                    }
                    // }}}

                    // {{{ Handle window resize
                    let handle_window_resize = Closure::wrap(Box::new(move |event: JsValue| {
                        #[derive(Debug, Deserialize)]
                        struct Event {
                            payload: WindowSize,
                        }

                        let window_size = from_value(event)
                            .map(|e: Event| e.payload)
                            .unwrap_or_default();

                        dispatch.reduce(|state| {
                            state
                                .map(|data| store::Data {
                                    window_size: window_size.clone(),
                                    ..data.clone()
                                })
                                .into()
                        });
                    })
                        as Box<dyn FnMut(JsValue)>);

                    js::onResized(handle_window_resize.into_js_value()).await;
                    // }}}

                    js::invoke("request_data", JsValue::NULL).await;
                });
            }

            || {}
        });
    }

    html! {
        <BrowserRouter>
        {
            match state.deref().clone() {
                State::Loading => {
                    html! {
                        <div class="container d-flex align-items-center" style="height: 100vh;">
                            <Message icon="fa-solid fa-database fa-bounce">{"Loading data..."}</Message>
                        </div>
                    }
                }
                State::LoadingFailed(msg) => {
                    html! {
                        <div class="container d-flex align-items-center" style="height: 100vh;">
                            <Message r#type={MessageType::Error}>{msg}</Message>
                        </div>
                    }
                }
                State::Ready(_) => html! {
                    <>
                        <Header />
                        <PageMessage />

                        <Switch<Route> render={switch} />
                    </>
                },
                State::Waiting(_) => html! {
                    <>
                        <Header disabled={true} />
                        <PageMessage />

                        <Switch<Route> render={switch} />
                    </>
                },
            }
        }

        </BrowserRouter>
    }
}
