/* ui/src/page/account_maintenance/mod.rs: Account maintenance page
 *
 * Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

mod archive;
mod form;
mod list;
mod restore;

pub use list::AccountMaintenance;
