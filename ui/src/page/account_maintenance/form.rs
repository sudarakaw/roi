/* ui/src/page/account_maintenance/form.rs: Account add/edit dialog
 *
 * Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

use std::ops::Deref;

use roi::db::{Account, AccountType};
use serde::Serialize;
use serde_wasm_bindgen::{from_value, to_value};
use wasm_bindgen::JsCast;
use web_sys::{HtmlInputElement, InputEvent, MouseEvent};
use yew::{function_component, html, platform::spawn_local, use_state, Callback, Html, Properties};
use yewdux::prelude::use_store;

use crate::{
    component::{Dialog, DialogButtons, DialogMessage},
    js,
    store::{self, State},
};

#[derive(Properties, PartialEq)]
pub struct FormProps {
    pub account: Option<Account>,
    pub disabled: bool,

    pub onclose: Callback<MouseEvent>,
}

#[function_component]
pub fn AccountForm(props: &FormProps) -> Html {
    let (state, dispatch) = use_store::<State>();
    let message = use_state(|| DialogMessage::None);
    let selected_portfolio = use_state(|| state.selected_portfolio().unwrap());
    let account = use_state(|| {
        props.account.clone().unwrap_or_else(|| {
            Account::new(
                selected_portfolio.deref().id.clone(),
                AccountType::default(),
                "".to_string(),
                false,
                365,
            )
        })
    });

    let handle_dpy_change = {
        let account = account.clone();

        Callback::from(move |event: InputEvent| {
            let mut data = account.deref().clone();
            let value = event
                .target()
                .unwrap()
                .unchecked_into::<HtmlInputElement>()
                .value()
                .parse();

            if let Ok(value) = value {
                data.days_per_year = value;
                account.set(data);
            }
        })
    };

    let handle_number_change = {
        let account = account.clone();

        Callback::from(move |event: InputEvent| {
            let mut data = account.deref().clone();
            let value = event
                .target()
                .unwrap()
                .unchecked_into::<HtmlInputElement>()
                .value();

            data.number = value;
            account.set(data);
        })
    };

    let handle_type_change = {
        let account = account.clone();

        Callback::from(move |event: InputEvent| {
            let mut data = account.deref().clone();
            let value = event
                .target()
                .unwrap()
                .unchecked_into::<HtmlInputElement>()
                .value();

            data.account_type = serde_json::from_str(&value).unwrap_or_default();
            account.set(data);
        })
    };

    let submit_form = |new: bool| {
        let account = account.clone();
        let message = message.clone();
        let onclose = props.onclose.clone();

        Callback::from(move |_| {
            let dispatch = dispatch.clone();
            let account = account.clone();
            let message = message.clone();
            let onclose = onclose.clone();

            spawn_local(async move {
                #[derive(Serialize)]
                struct Args {
                    account: Account,
                }

                dispatch.reduce(|state| state.as_waiting().into());

                let account = account.deref().clone();
                let args = to_value(&Args {
                    account: account.clone(),
                })
                .unwrap();
                let result: Option<String> =
                    from_value(js::invoke("save_account", args).await).unwrap();

                if let Some(msg) = result {
                    message.set(DialogMessage::Error(msg.into()));

                    dispatch.reduce(|state| state.as_ready().into());
                } else if new {
                    dispatch.reduce(|state| {
                        state
                            .map(|data| store::Data {
                                page_message: store::PageMessage::Success(
                                    format!("Account \"{}\" created successfully.", account.number)
                                        .into(),
                                ),
                                ..data.clone()
                            })
                            .into()
                    });

                    onclose.emit(MouseEvent::new("").unwrap());
                } else {
                    message.set(DialogMessage::Success("Successfully updated.".into()));
                }
            })
        })
    };

    let title = if props.account.is_some() {
        "Existing Account".to_string()
    } else {
        "New Account".to_string()
    };

    let buttons = DialogButtons::Inclusive(if props.account.is_some() {
        html! {
            <button type="button" class="btn btn-success" onclick={submit_form(false)} disabled={props.disabled}>
                <i class="fa-solid fa-floppy-disk"></i>
                <span>{"Update"}</span>
            </button>
        }
    } else {
        html! {
            <button type="button" class="btn btn-success" onclick={submit_form(true)} disabled={props.disabled}>
                <i class="fa-solid fa-floppy-disk"></i>
                <span>{"Create"}</span>
            </button>
        }
    });

    let message = message.deref().clone();

    html! {
        <Dialog onclose={props.onclose.clone()} {buttons} {title} {message} disabled={props.disabled}>
            <form>
                <fieldset disabled={props.disabled}>
                    <div class="form-floating mb-3">
                        <input type="text" class="form-control" id="account_number" placeholder="Name" value={account.number.clone()} oninput={handle_number_change} />
                        <label for="account_number">{"Number"}</label>
                    </div>

                    <div class="form-floating mb-3">
                        <select class="form-select" id="account_type" oninput={handle_type_change}>
                        { account_type_options(account.account_type.clone()) }
                        </select>
                        <label for="account_type">{"Type"}</label>
                    </div>

                    <div class="form-control border-0">
                        <label for="dpy">{format!("Days per Year: {}", account.days_per_year)}</label>
                        <input
                            type="range"
                            class="form-range"
                            id="dpy"
                            placeholder="Days per Year"
                            min="364"
                            max="366"
                            value={account.days_per_year.to_string()}
                            oninput={handle_dpy_change} />
                    </div>
                </fieldset>
            </form>
        </Dialog>
    }
}

fn account_type_options(selected_value: AccountType) -> Html {
    AccountType::list()
        .iter()
        .map(|t| {
            let selected = selected_value == *t;

            html! { <option {selected} value={serde_json::to_string(t).unwrap()}>{ t.to_string() }</option> }
        })
        .collect()
}
