/* ui/src/page/account_maintenance/list.rs: Account listing page
 *
 * Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

use std::ops::Deref;

use roi::db::Account;
use web_sys::MouseEvent;
use yew::{classes, function_component, html, use_state, Callback, Html, Properties};
use yewdux::prelude::use_store;

use crate::{
    component::{Message, MessageType},
    page::PageDialog,
    store::State,
    util::number_format,
};

use super::archive::ArchiveConfirm;
use super::form::AccountForm;
use super::restore::RestoreConfirm;

#[function_component]
pub fn AccountMaintenance() -> Html {
    let dialog = use_state(|| PageDialog::<Account>::None);
    let show_archived = use_state(|| false);
    let (state, _) = use_store::<State>();
    let mut all_accounts = state.data().map(|d| d.accounts.clone()).unwrap_or(vec![]);

    all_accounts.sort_by(|a, b| {
        let result = a.account_type.cmp(&b.account_type);

        if result.is_eq() {
            return a.number.cmp(&b.number);
        }

        result
    });

    let visible_accounts = all_accounts
        .clone()
        .into_iter()
        .filter(|a| !a.archived || *show_archived)
        .collect::<Vec<Account>>();
    let disabled = !matches!(*state, State::Ready(_));

    let toggle_archived = {
        let show_archived = show_archived.clone();

        Callback::from(move |_| {
            show_archived.set(!(*show_archived));
        })
    };

    let show_form = |account: Option<Account>| {
        let dialog = dialog.clone();

        Callback::from(move |_| {
            dialog.set(PageDialog::Form(account.clone()));
        })
    };

    let show_confirm_archive = |account: Account| {
        let dialog = dialog.clone();

        Callback::from(move |_| {
            dialog.set(PageDialog::DeleteConfirm(account.clone()));
        })
    };

    let hide_dialog = {
        let dialog = dialog.clone();

        Callback::from(move |_| {
            dialog.set(PageDialog::None);
        })
    };

    let dialog = match dialog.deref() {
        PageDialog::Form(option_account) => {
            html! { <AccountForm account={option_account.clone()} onclose={hide_dialog} {disabled} /> }
        }
        PageDialog::DeleteConfirm(account) => {
            if account.archived {
                html! { <RestoreConfirm account={account.clone()} onclose={hide_dialog} {disabled} /> }
            } else {
                html! { <ArchiveConfirm account={account.clone()} onclose={hide_dialog} {disabled} /> }
            }
        }
        _ => html! {},
    };

    let mut archive_toggle_icon_classs = classes!("me-md-1");

    if *show_archived {
        archive_toggle_icon_classs.push("fa-solid fa-circle-check");
    } else {
        archive_toggle_icon_classs.push("fa-regular fa-circle");
    };

    html! {
        <>

        { dialog }

        <div class="container table-responsive mt-4 account-maintenance">
            <table class="table table-hover caption-top">
                <caption><h4>{"Account Maintenance"}</h4></caption>

                { if !all_accounts.is_empty() { html! {
                    <>

                    <thead>
                        <tr>
                            <th class="col-2"></th>
                            <th class="col-6 col-md-3 col-lg-4"></th>
                            <th class="d-none d-md-table-cell col-3 col-lg-2"></th>
                            <th class="col-1"></th>
                            <th class="col-1"></th>
                            <th class="col-3">
                                <div class="buttons archive-toggle">
                                    <input type="checkbox" class="btn-check" id="btn-check-outlined" autocomplete="off" checked={*show_archived} oninput={toggle_archived} />
                                    <label class="btn btn-sm btn-outline-dark" for="btn-check-outlined">
                                        <i class={archive_toggle_icon_classs}></i>
                                        <span class="d-none d-xl-inline">{"Show Archived"}</span>
                                    </label>
                                </div>
                            </th>
                        </tr>

                        { if !visible_accounts.is_empty() { html! {
                        <tr>
                            <th>{"Type"}</th>
                            <th>{"Number"}</th>
                            <th class="d-none d-md-table-cell text-end">{"Investment"}</th>
                            <th class="text-end">{"Rate"}</th>
                            <th class="text-end">{"Day/Yr."}</th>
                            <th>
                                <div class="buttons">
                                    <button
                                        type="button"
                                        {disabled}
                                        onclick={show_form(None)}
                                        class="btn btn-sm btn-light text-primary float-end">
                                        <i class="fa fa-plus"></i>
                                        {"New"}
                                    </button>
                                </div>
                            </th>
                        </tr>
                        }
                        } else { html!{} } }

                    </thead>

                    <tbody class="table-group-divider">
                    {
                        if visible_accounts.is_empty() {
                            html! {
                                <tr class="table-warning">
                                    <td colspan="2">
                                        <span>{"No active accounts found in current portfolio."}</span>
                                        <br />
                                        <small class="text-secondary">{"There are some archived accounts not visible."}</small>
                                    </td>
                                    <td>
                                        <button
                                            type="button"
                                            class="btn btn-sm btn-warning"
                                            onclick={ show_form(None) }>
                                            {"Create New Account"}
                                        </button>
                                    </td>
                                </tr>
                            }
                        }
                        else {
                            visible_accounts
                                .iter()
                                .map(|account| html! {
                                    <AccountRow
                                        account={account.clone()}
                                        {disabled}
                                        onedit={show_form(Some(account.clone()))}
                                        onarchive={show_confirm_archive(account.clone())}
                                    />
                                })
                                .collect::<Html>()
                        }
                    }
                    </tbody>

                    </>
                } } else { html! {} } }
            </table>

            { if all_accounts.is_empty() { html! { <NoAccounts onclick={show_form(None)} /> } } else { html!{} } }
        </div>

        </>
    }
}

#[derive(Properties, PartialEq)]
struct NoAccountsProps {
    onclick: Callback<MouseEvent>,
}

#[function_component]
fn NoAccounts(props: &NoAccountsProps) -> Html {
    html! {
        <Message r#type={MessageType::Warning}>
            <div class="d-flex gap-2 justify-content-center align-items-center">
                <span>{"No accounts found in current portfolio. Click here to "}</span>
                <button
                    type="button"
                    class="btn btn-sm btn-warning"
                    onclick={ props.onclick.clone() }>
                    {"Create New Account"}
                </button>
            </div>
        </Message>
    }
}

#[derive(Properties, PartialEq)]
struct RowProps {
    account: Account,
    disabled: bool,
    onedit: Callback<MouseEvent>,
    onarchive: Callback<MouseEvent>,
}

#[function_component]
fn AccountRow(props: &RowProps) -> Html {
    let (state, _) = use_store::<State>();
    let account = props.account.clone();
    let transaction = state
        .deref()
        .data()
        .and_then(|d| d.transactions.get(&account.get_id()));

    let archive_text;
    let mut archive_icon = classes!("fa-solid");
    let mut archive_class = classes!("btn", "btn-sm", "btn-light", "text-truncate");

    if account.archived {
        archive_text = "Restore";
        archive_icon.push("fa-box-open");
        archive_class.push("text-success");
    } else {
        archive_text = "Archive";
        archive_icon.push("fa-box-archive");
        archive_class.push("text-danger");
    };

    html! {
        <tr>
            <td class="text-truncate">{account.account_type.to_string()}</td>
            <td>{account.number.clone()}</td>
            <td class="d-none d-md-table-cell text-end font-monospace">
            {
                if let Some(transaction) = transaction {
                    html! { number_format(transaction.amount, ",") }
                }
                else { html! {} }
            }
            </td>
            <td class="text-end font-monospace">
            {
                if let Some(transaction) = transaction {
                    html! { format!("{}%", number_format(transaction.rate.into(), ",")) }
                }
                else { html! {} }
            }
            </td>
            <td class="text-end
            font-monospace">{account.days_per_year.to_string()}</td>
            <td>
                <div class="buttons">
                    <button
                        type="button"
                        disabled={props.disabled}
                        onclick={props.onedit.clone()}
                        class="btn btn-sm btn-light text-dark">
                        <i class="fa-regular fa-pen-to-square"></i>
                        <span class="d-none d-xl-inline">{"Edit"}</span>
                    </button>
                    <button
                        type="button"
                        disabled={props.disabled}
                        onclick={props.onarchive.clone()}
                        class={ archive_class }>
                        <i class={ archive_icon }></i>
                        <span class="d-none d-xl-inline">{ archive_text }</span>
                    </button>
                </div>
            </td>
        </tr>

    }
}
