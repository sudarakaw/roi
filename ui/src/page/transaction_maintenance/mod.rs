/* ui/src/page/transaction_maintenance/mod.rs: Transaction maintenance page
 *
 * Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

mod form;
mod list;

use std::ops::Deref;

use roi::db::{Account, AccountId, Transaction};

use serde::Serialize;
use serde_wasm_bindgen::{from_value, to_value};
use yew::{
    classes, function_component, html, platform::spawn_local, use_effect_with, use_state, Callback,
    Html, Properties,
};
use yew_router::prelude::Link;
use yewdux::prelude::use_store;

use crate::{
    component::{Message, MessageType},
    js,
    page::{PageDialog, Route},
    store::State,
};

use form::TransactionForm;
use list::TransactionList;

#[derive(Clone, PartialEq)]
enum ComponentState {
    NoAccount,
    Loading(Account),
    Ready(Account, Vec<Transaction>),
}

#[derive(Properties, PartialEq)]
pub struct TransactionMaintenanceProps {
    pub accountid: AccountId,
}

#[function_component]
pub fn TransactionMaintenance(props: &TransactionMaintenanceProps) -> Html {
    let (state, _) = use_store::<State>();
    let disabled = !matches!(*state, State::Ready(_));
    let dialog_state = use_state(|| PageDialog::<form::PropTransaction>::None);
    let cstate = use_state(|| {
        state
            .deref()
            .data()
            .and_then(|d| {
                d.accounts
                    .clone()
                    .into_iter()
                    .find(|a| a.get_id() == props.accountid)
            })
            .map(ComponentState::Loading)
            .unwrap_or(ComponentState::NoAccount)
    });

    {
        let cstate = cstate.clone();
        let is_loading = matches!(cstate.deref().clone(), ComponentState::Loading(_));

        use_effect_with(is_loading, move |_| {
            if let ComponentState::Loading(account) = cstate.deref() {
                let account = account.clone();

                spawn_local(async move {
                    #[derive(Serialize)]
                    struct Args {
                        accountid: AccountId,
                    }

                    let args = to_value(&Args {
                        accountid: account.get_id(),
                    })
                    .unwrap();

                    let ts: Vec<Transaction> =
                        from_value(js::invoke("get_transactions", args).await).unwrap_or(vec![]);

                    cstate.set(ComponentState::Ready(account.clone(), ts));
                });
            }
        });
    }

    let reload_transactions = {
        let cstate = cstate.clone();

        Callback::from(move |_| {
            if let ComponentState::Ready(account, _) = cstate.deref() {
                cstate.set(ComponentState::Loading(account.clone()));
            }
        })
    };

    let show_add_form = {
        let dialog_state = dialog_state.clone();
        let cstate = cstate.clone();

        Callback::from(move |_| {
            let current_transaction = form::PropTransaction::New(
                // Get current transaction from the transaction list
                match cstate.deref() {
                    ComponentState::Ready(_, ts) => ts.iter().find(|t| t.is_current()),
                    _ => None,
                }
                // Calculate next maturity when available
                .map(|t| {
                    Transaction::new(
                        t.get_account_id(),
                        t.amount,
                        t.rate,
                        t.wht_rate,
                        t.get_maturity_date(),
                        t.days_to_maturity,
                    )
                }),
            );

            dialog_state.set(PageDialog::Form(Some(current_transaction)));
        })
    };

    let show_edit_form = {
        let dialog_state = dialog_state.clone();

        Callback::from(move |transaction: Transaction| {
            dialog_state.set(PageDialog::Form(Some(form::PropTransaction::Edit(
                transaction,
            ))));
        })
    };

    let hide_dialog = {
        let dialog_state = dialog_state.clone();

        Callback::from(move |_| {
            dialog_state.set(PageDialog::None);
        })
    };

    let dialog = match dialog_state.deref() {
        PageDialog::Form(option_transaction) => {
            let transaction = option_transaction
                .clone()
                .unwrap_or(form::PropTransaction::New(None));

            html! { <TransactionForm accountid={ props.accountid } { transaction } onclose={hide_dialog} onsave={reload_transactions} {disabled} /> }
        }
        _ => html! {},
    };

    html! {
        <>

        { dialog }

        <div class="container table-responsive mt-4 transaction-maintenance">
            <table class="table table-hover caption-top">
                { if let ComponentState::Ready(account, transactions) = cstate.deref() {
                    html! {
                        <>

                        <caption>
                            <h4>
                                {"Transaction Maintenance: "}
                                <small class="text-secondary">
                                    {account.account_type.to_string()}
                                    {" Account - "}
                                    {account.number.clone()}
                                </small>
                            </h4>
                        </caption>

                        <TransactionList
                            account={ account.clone() }
                            transactions={ transactions.clone() }
                            { disabled }
                            onadd={ show_add_form }
                            onedit={ show_edit_form } />

                        </>
                    }
                }
                else { html! {} }
                }
            </table>

            {
                match cstate.deref() {
                    ComponentState::NoAccount => html! { <NoAccount /> },
                    ComponentState::Loading(_) => html! { <LoadingData /> },
                    _ => html! {}
                }
            }
        </div>

        </>
    }
}

#[function_component]
fn LoadingData() -> Html {
    html! {
        <Message r#type={MessageType::Info} icon={Some("me-3 fa-solid fa-database fa-bounce")}>
            {"Loading transactions..."}
        </Message>
    }
}

#[function_component]
fn NoAccount() -> Html {
    html! {
        <Message r#type={MessageType::Error} icon={Some("me-3 fs-1 align-self-start fa-solid fa-circle-exclamation")}>
            <strong class="fs-4">{"Account not found."}</strong>
            <p class="lead">
                {"Please verify the account exists in "}
                <Link<Route>
                    to={Route::AccountMaintenance}
                    classes={classes!("btn", "btn-sm", "btn-outline-danger")}>
                    {"Accounts"}
                </Link<Route>>
                {" or "}
                <Link<Route>
                    to={Route::Summary}
                    classes={classes!("btn", "btn-sm", "btn-outline-danger")}>
                    {"Sumamry"}
                </Link<Route>>
                {" page."}
            </p>
        </Message>
    }
}
