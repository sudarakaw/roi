/* ui/src/page/transaction_maintenance/form.rs: Transaction add/edit dialog
 *
 * Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

use std::ops::{Add, Deref};

use chrono::{Days, Local, NaiveDate};
use roi::db::{AccountId, Transaction};
use serde::Serialize;
use serde_wasm_bindgen::{from_value, to_value};
use wasm_bindgen::JsCast;
use web_sys::{HtmlInputElement, InputEvent, MouseEvent};
use yew::{
    function_component, html, platform::spawn_local, use_state, Callback, Html, Properties,
    UseStateHandle,
};
use yewdux::prelude::use_store;

use crate::{
    component::{Dialog, DialogButtons, DialogMessage},
    js,
    store::{self, State},
    util::{from_formatted, number_format},
};

#[derive(Clone, Debug)]
struct FormData {
    transaction: Transaction,
    amount: String,
    invested_on: String,
    rate: String,
    wht_rate: String,
    days_to_maturity: u16,
}

impl From<Transaction> for FormData {
    fn from(transaction: Transaction) -> Self {
        Self {
            transaction: transaction.clone(),
            amount: number_format(transaction.amount, ","),
            invested_on: transaction.invested_on.format("%Y-%m-%d").to_string(),
            rate: number_format(transaction.rate.into(), ""),
            wht_rate: number_format(transaction.wht_rate.into(), ""),
            days_to_maturity: transaction.days_to_maturity,
        }
    }
}

impl From<FormData> for Transaction {
    fn from(form: FormData) -> Self {
        let mut transaction = form.transaction;

        transaction.amount = from_formatted(&form.amount);
        transaction.rate = from_formatted(&form.rate);
        transaction.wht_rate = from_formatted(&form.wht_rate);
        transaction.invested_on = NaiveDate::parse_from_str(&form.invested_on, "%Y-%m-%d").unwrap();
        transaction.days_to_maturity = form.days_to_maturity;

        transaction
    }
}

#[derive(Clone, PartialEq)]
pub enum PropTransaction {
    New(Option<Transaction>),
    Edit(Transaction),
}

impl PropTransaction {
    fn get(&self) -> Option<&Transaction> {
        match self {
            Self::New(ot) => ot.as_ref(),
            Self::Edit(t) => Some(t),
        }
    }

    fn is_new(&self) -> bool {
        match self {
            Self::New(_) => true,
            Self::Edit(_) => false,
        }
    }
}

#[derive(Properties, PartialEq)]
pub struct FormProps {
    pub accountid: AccountId,
    pub transaction: PropTransaction,
    pub disabled: bool,

    pub onclose: Callback<MouseEvent>,
    pub onsave: Callback<MouseEvent>,
}

#[function_component]
pub fn TransactionForm(props: &FormProps) -> Html {
    let (_, dispatch) = use_store::<State>();
    let message = use_state(|| DialogMessage::None);
    let transaction: UseStateHandle<FormData> = use_state(|| {
        props
            .transaction
            .get()
            .cloned()
            .unwrap_or_else(|| {
                Transaction::new(
                    props.accountid,
                    0,
                    0,
                    500,
                    Local::now().naive_local().into(),
                    90,
                )
            })
            .into()
    });

    let handle_amount_change = {
        let transaction = transaction.clone();

        Callback::from(move |event: InputEvent| {
            let mut data = transaction.deref().clone();
            let value = event
                .target()
                .unwrap()
                .unchecked_into::<HtmlInputElement>()
                .value();

            data.amount = value;
            transaction.set(data.clone());
        })
    };

    let handle_date_change = {
        let transaction = transaction.clone();

        Callback::from(move |event: InputEvent| {
            let value = event
                .target()
                .unwrap()
                .unchecked_into::<HtmlInputElement>()
                .value();

            let mut data = transaction.deref().clone();

            data.invested_on = value;
            transaction.set(data);
        })
    };

    let handle_rate_change = {
        let transaction = transaction.clone();

        Callback::from(move |event: InputEvent| {
            let mut data = transaction.deref().clone();
            let value = event
                .target()
                .unwrap()
                .unchecked_into::<HtmlInputElement>()
                .value();

            data.rate = value;
            transaction.set(data);
        })
    };

    let handle_wht_rate_change = {
        let transaction = transaction.clone();

        Callback::from(move |event: InputEvent| {
            let mut data = transaction.deref().clone();
            let value = event
                .target()
                .unwrap()
                .unchecked_into::<HtmlInputElement>()
                .value();

            data.wht_rate = value;
            transaction.set(data);
        })
    };

    let handle_days_change = {
        let transaction = transaction.clone();

        Callback::from(move |event: InputEvent| {
            let mut data = transaction.deref().clone();
            let value = event
                .target()
                .unwrap()
                .unchecked_into::<HtmlInputElement>()
                .value()
                .parse();

            if let Ok(value) = value {
                data.days_to_maturity = value;
                transaction.set(data);
            }
        })
    };

    let submit_form = |_new: bool| {
        let transaction = transaction.clone();
        let message = message.clone();
        let onclose = props.onclose.clone();
        let onsave = props.onsave.clone();

        Callback::from(move |_| {
            let transaction = transaction.clone();
            let dispatch = dispatch.clone();
            let message = message.clone();
            let onclose = onclose.clone();
            let onsave = onsave.clone();

            spawn_local(async move {
                #[derive(Serialize)]
                struct Args {
                    transaction: Transaction,
                }

                dispatch.reduce(|state| state.as_waiting().into());

                let transaction: Transaction = transaction.deref().clone().into();
                let args = to_value(&Args {
                    transaction: transaction.clone(),
                })
                .unwrap();
                let result: Option<String> =
                    from_value(js::invoke("save_transaction", args).await).unwrap();

                if let Some(msg) = result {
                    message.set(DialogMessage::Error(msg.into()));

                    dispatch.reduce(|state| state.as_ready().into());
                } else {
                    dispatch.reduce(|state| {
                        state
                            .map(|data| store::Data {
                                page_message: store::PageMessage::Success(
                                    "Transaction created successfully.".into(),
                                ),
                                ..data.clone()
                            })
                            .into()
                    });

                    onsave.emit(MouseEvent::new("").unwrap());
                    onclose.emit(MouseEvent::new("").unwrap());
                }
            })
        })
    };

    let title = if props.transaction.is_new() {
        "New Transaction"
    } else {
        "Existing Transaction"
    };

    let buttons = DialogButtons::Inclusive(if props.transaction.is_new() {
        html! {
            <button type="button" class="btn btn-success" onclick={submit_form(true)} disabled={props.disabled}>
                <i class="fa-solid fa-floppy-disk"></i>
                <span>{"Create"}</span>
            </button>
        }
    } else {
        html! {
            <button type="button" class="btn btn-success" onclick={submit_form(false)} disabled={props.disabled}>
                <i class="fa-solid fa-floppy-disk"></i>
                <span>{"Update"}</span>
            </button>
        }
    });

    let message = message.deref().clone();
    let today_text = Local::now()
        .add(Days::new(1))
        .format("%Y-%m-%d")
        .to_string();

    html! {
        <Dialog onclose={props.onclose.clone()} {buttons} {title} {message} disabled={props.disabled}>
            <form>
                <fieldset disabled={props.disabled}>
                    <div class="row">
                        <div class="col">
                            <div class="form-floating mb-3">
                                <input
                                    type="text"
                                    inputmode="numeric"
                                    pattern="[0-9]+([\\.,][0-9]+)?"
                                    class="form-control"
                                    id="invested_amount"
                                    placeholder="Invested Amount"
                                    value={transaction.amount.clone()}
                                    step=".01"
                                    oninput={handle_amount_change} />
                                <label for="invested_amount">{"Invested Amount"}</label>
                            </div>
                        </div>

                        <div class="col">
                            <div class="form-floating">
                                <input
                                    type="date"
                                    class="form-control"
                                    id="invested_date"
                                    placeholder="Invested Date"
                                    value={transaction.invested_on.clone()}
                                    max={today_text}
                                    oninput={handle_date_change} />
                                <label for="invested_date">{"Invested Date"}</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="form-floating mb-3">
                                <input
                                    type="number"
                                    class="form-control"
                                    id="intrest_rate"
                                    placeholder="Intrest Rate"
                                    value={transaction.rate.clone()}
                                    step=".05"
                                    max="100"
                                    min="0"
                                    oninput={handle_rate_change} />
                                <label for="intrest_rate">{"Intrest Rate"}</label>
                            </div>
                        </div>

                        <div class="col">
                            <label for="days">{format!("Days to Mature: {}", transaction.days_to_maturity)}</label>
                            <input
                                type="range"
                                list="preset_days"
                                class="form-range"
                                id="days"
                                value={transaction.days_to_maturity.to_string()}
                                min="25"
                                max="366"
                                oninput={handle_days_change} />
                            <datalist id="preset_days">
                                <option value="30" />
                                <option value="88" />
                                <option value="89" />
                                <option value="90" />
                                <option value="91" />
                                <option value="92" />
                            </datalist>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="form-floating mb-3">
                                <input
                                    type="number"
                                    class="form-control"
                                    id="wht_rate"
                                    placeholder="WHT Rate"
                                    value={transaction.wht_rate.clone()}
                                    step=".05"
                                    max="100"
                                    min="0"
                                    oninput={handle_wht_rate_change} />
                                <label for="wht_rate">{"WHT Rate"}</label>
                            </div>
                        </div>

                        <div class="col"></div>
                    </div>
                </fieldset>
            </form>
        </Dialog>
    }
}
