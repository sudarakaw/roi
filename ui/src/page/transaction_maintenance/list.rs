/* ui/src/page/transaction_maintenance/list.rs: Transaction listing module
 *
 * Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

use chrono::Datelike;
use roi::db::{Account, Transaction};

use serde_json::to_value;
use web_sys::MouseEvent;
use yew::{classes, function_component, html, Callback, Html, Properties};

use crate::{
    component::{Message, MessageType},
    util::{number_format, number_suffix},
};

#[derive(Properties, PartialEq)]
pub struct TransactionListProps {
    pub account: Account,
    pub transactions: Vec<Transaction>,
    pub disabled: bool,
    pub onadd: Callback<MouseEvent>,
    pub onedit: Callback<Transaction>,
}

#[function_component]
pub fn TransactionList(props: &TransactionListProps) -> Html {
    match props.transactions.as_slice() {
        [] => {
            html! { <NoTransactions account={ props.account.clone() } onclick={ props.onadd.clone() } />  }
        }
        ts => html! {
            <>

            <thead>
                <tr>
                    <th>{"Invested Date"}</th>
                    <th class="d-none d-md-table-cell text-end">{"Invested Amount"}</th>
                    <th class="d-none d-lg-table-cell text-end">{"Rate"}</th>
                    <th class="d-none d-lg-table-cell text-end">{"WHT %"}</th>
                    <th class="d-none d-md-table-cell text-end">{"Period"}</th>
                    <th>{"Maturity"}</th>
                    <th>
                        <div class="buttons">
                            <button
                                type="button"
                                disabled={props.disabled}
                                onclick={props.onadd.clone()}
                                class="btn btn-sm btn-light text-primary float-end">
                                <i class="fa fa-plus"></i>
                                {"New"}
                            </button>
                        </div>
                    </th>
                </tr>
            </thead>

            <tbody class="table-group-divider">
            {
                ts.iter().map(|t| html! { <TransactionRow transaction={t.clone()} disabled={props.disabled} onedit={props.onedit.clone()} /> }).collect::<Html>()
            }
            </tbody>

            </>
        },
    }
}

#[derive(Properties, PartialEq)]
struct TransactionRowProps {
    transaction: Transaction,
    disabled: bool,
    onedit: Callback<Transaction>,
}

#[function_component]
fn TransactionRow(props: &TransactionRowProps) -> Html {
    let transaction = props.transaction.clone();

    let invested_date_1 = transaction.invested_on.format("%Y, %b %d");
    let invested_date_2 = number_suffix(transaction.invested_on.day());
    let invested_date_3 = transaction.invested_on.format(" (%a)");

    let maturity_date = transaction.get_maturity_date();
    let maturity_date_1 = maturity_date.format("%Y, %b %d");
    let maturity_date_2 = number_suffix(maturity_date.day());
    let maturity_date_3 = maturity_date.format(" (%a)");

    let mut row_classes = classes!();

    if transaction.is_current() {
        row_classes.push("table-primary");
    }

    let transaction_id = to_value(transaction.get_id())
        .map(|v| v.to_string())
        .unwrap_or("".to_string());

    let on_edit = props.onedit.clone();

    html! {
        <tr class={row_classes}>
            <td class="text-truncate">{invested_date_1}<sup>{invested_date_2}</sup>{invested_date_3}</td>
            <td class="d-none d-md-table-cell font-monospace text-end">{number_format(transaction.amount, ",")}</td>
            <td class="d-none d-lg-table-cell font-monospace text-end">{number_format(transaction.rate.into(), ",")}{"%"}</td>
            <td class="d-none d-lg-table-cell font-monospace text-end">{number_format(transaction.wht_rate.into(), ",")}{"%"}</td>
            <td class="d-none d-md-table-cell text-end">{transaction.days_to_maturity}{" days"}</td>
            <td class="text-truncate">{maturity_date_1}<sup>{maturity_date_2}</sup>{maturity_date_3}</td>
            <td>
                <div class="buttons">
                    {
                        if transaction.is_current()  {
                            html! {
                                <button
                                    type="button"
                                    disabled={props.disabled}
                                    data-id={transaction_id}
                                    onclick={Callback::from(move |_| on_edit.emit(transaction.clone()))}
                                    class="btn btn-sm btn-light text-dark">
                                    <i class="fa-regular fa-pen-to-square"></i>
                                    <span class="d-none d-md-inline">{"Edit"}</span>
                                </button>
                            }
                        }
                        else { html! {} }
                    }
                </div>
            </td>
        </tr>
    }
}

#[derive(Properties, PartialEq)]
struct NoTransactionsProps {
    account: Account,

    onclick: Callback<MouseEvent>,
}

#[function_component]
fn NoTransactions(props: &NoTransactionsProps) -> Html {
    html! {
        <Message r#type={MessageType::Warning}>
            <div class="d-flex gap-2 justify-content-center align-items-center">
                <span>{format!("No transactions found for {} account {}. Click here to ", props.account.account_type, props.account.number )}</span>
                <button
                    type="button"
                    class="btn btn-sm btn-warning"
                    onclick={ props.onclick.clone() }>
                    {"Create New Transaction"}
                </button>
            </div>
        </Message>
    }
}
