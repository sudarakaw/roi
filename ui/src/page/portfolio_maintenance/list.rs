/* ui/src/page/portfolio_maintenance/list.rs: Portfolio listing page
 *
 * Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

use std::ops::Deref;

use roi::db::Portfolio;
use serde::Serialize;
use serde_wasm_bindgen::to_value;
use web_sys::MouseEvent;
use yew::{
    classes, function_component, html, platform::spawn_local, use_state, Callback, Html, Properties,
};
use yewdux::prelude::use_store;

use crate::{js, page::PageDialog, store::State};

use super::{delete::DeleteConfirm, form::PortfolioForm};

#[function_component]
pub fn PortfolioMaintenance() -> Html {
    let dialog = use_state(|| PageDialog::<Portfolio>::None);
    let (state, dispatch) = use_store::<State>();
    let portfolios = state.data().map(|d| d.portfolios.clone()).unwrap_or(vec![]);
    let disabled = !matches!(*state, State::Ready(_));

    let select_change_handler = |portfolio: &Portfolio| {
        let dispatch = dispatch.clone();
        let portfolio = portfolio.clone();

        Callback::from(move |event: MouseEvent| {
            let dispatch = dispatch.clone();
            let portfolio = portfolio.clone();

            event.prevent_default();

            spawn_local(async move {
                #[derive(Serialize)]
                struct Args {
                    id: String,
                }

                dispatch.reduce(|state| state.as_waiting().into());

                let args = to_value(&Args {
                    id: portfolio.id.into(),
                })
                .unwrap();

                js::invoke("change_portfolio", args).await;
            });
        })
    };

    let show_form = |portfolio: Option<Portfolio>| {
        let dialog = dialog.clone();

        Callback::from(move |_| {
            dialog.set(PageDialog::Form(portfolio.clone()));
        })
    };

    let show_confirm_delete = |portfolio: Portfolio| {
        let dialog = dialog.clone();

        Callback::from(move |_| {
            dialog.set(PageDialog::DeleteConfirm(portfolio.clone()));
        })
    };

    let hide_dialog = {
        let dialog = dialog.clone();

        Callback::from(move |_| {
            dialog.set(PageDialog::None);
        })
    };

    let dialog = match dialog.deref() {
        PageDialog::Form(option_portfolio) => {
            html! { <PortfolioForm portfolio={option_portfolio.clone()} onclose={hide_dialog} {disabled} /> }
        }
        PageDialog::DeleteConfirm(portfolio) => {
            html! { <DeleteConfirm portfolio={portfolio.clone()} onclose={hide_dialog} {disabled} /> }
        }
        _ => html! {},
    };

    html! {
        <>

        { dialog }

        <div class="container table-responsive mt-4 portfolio-maintenance">
            <table class="table table-hover caption-top">
                <caption><h4>{"Portfolio Maintenance"}</h4></caption>
                <thead>
                    <tr>
                        <th class="col-8 col-lg-9">{"Name"}</th>
                        <th class="col-1 text-center">{"Selected"}</th>
                        <th class="col-3 col-lg-2">
                            <div class="buttons">
                                <button
                                    type="button"
                                    {disabled}
                                    onclick={show_form(None)}
                                    class="btn btn-sm btn-light text-primary float-end btn-create-portfolio">
                                    <i class="fa fa-plus"></i>
                                    <span class="">{"New"}</span>
                                </button>
                            </div>
                        </th>
                    </tr>
                </thead>
                <tbody class="table-group-divider">
                {
                    portfolios
                        .iter()
                        .map(|portfolio| html! {
                            <PortfolioRow
                                portfolio={portfolio.clone()}
                                {disabled}
                                onselectchange={select_change_handler(portfolio)}
                                onedit={show_form(Some(portfolio.clone()))}
                                ondelete={show_confirm_delete(portfolio.clone())}
                            />
                        })
                        .collect::<Html>()
                }
                </tbody>
            </table>
        </div>

        </>
    }
}

#[derive(Properties, PartialEq)]
struct RowProps {
    portfolio: Portfolio,
    disabled: bool,
    onselectchange: Callback<MouseEvent>,
    onedit: Callback<MouseEvent>,
    ondelete: Callback<MouseEvent>,
}

#[function_component]
fn PortfolioRow(props: &RowProps) -> Html {
    let portfolio = props.portfolio.clone();
    let mut selected_state_class = classes!["fs-4"];

    if portfolio.selected {
        selected_state_class.push("fa-solid fa-circle-check text-success");
    } else {
        selected_state_class.push("fa-regular fa-circle text-secondary");
    }

    html! {
        <tr>
            <td>{portfolio.name.clone()}</td>
            <td class="text-center">
                <button
                    type="button"
                    disabled={props.disabled}
                    onclick={props.onselectchange.clone()}
                    class="btn btn-sm selected-state">
                    <i class={selected_state_class}></i>
                </button>
            </td>
            <td>
                <div class="buttons">
                    <button
                        type="button"
                        disabled={props.disabled}
                        onclick={props.onedit.clone()}
                        class="btn btn-sm btn-light text-dark">
                        <i class="fa-regular fa-pen-to-square"></i>
                        <span class="d-none d-md-inline">{"Edit"}</span>
                    </button>
                    <button
                        type="button"
                        disabled={props.disabled}
                        onclick={props.ondelete.clone()}
                        class="btn btn-sm btn-light text-danger">
                        <i class="fa-regular fa-trash-can"></i>
                        <span class="d-none d-md-inline">{"Delete"}</span>
                    </button>
                </div>
            </td>
        </tr>
    }
}
