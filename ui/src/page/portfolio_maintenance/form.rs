/* ui/src/page/portfolio_maintenance/form.rs: Portfolio add/edit dialog
 *
 * Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

use std::ops::Deref;

use roi::db::Portfolio;
use serde::Serialize;
use serde_wasm_bindgen::{from_value, to_value};
use wasm_bindgen::JsCast;
use web_sys::{HtmlInputElement, InputEvent, MouseEvent};
use yew::{function_component, html, platform::spawn_local, use_state, Callback, Html, Properties};
use yewdux::prelude::use_store;

use crate::{
    component::{Dialog, DialogButtons, DialogMessage},
    js,
    store::{self, State},
};

#[derive(Properties, PartialEq)]
pub struct FormProps {
    pub portfolio: Option<Portfolio>,
    pub disabled: bool,

    pub onclose: Callback<MouseEvent>,
}

#[function_component]
pub fn PortfolioForm(props: &FormProps) -> Html {
    let (_, dispatch) = use_store::<State>();
    let message = use_state(|| DialogMessage::None);
    let portfolio = use_state(|| {
        props
            .portfolio
            .clone()
            .unwrap_or(Portfolio::new("".to_string(), false))
    });

    let handle_name_change = {
        let portfolio = portfolio.clone();

        Callback::from(move |event: InputEvent| {
            let mut data = portfolio.deref().clone();
            let value = event
                .target()
                .unwrap()
                .unchecked_into::<HtmlInputElement>()
                .value();

            data.name = value;
            portfolio.set(data);
        })
    };

    let handle_selected_change = {
        let portfolio = portfolio.clone();

        Callback::from(move |event: InputEvent| {
            let mut data = portfolio.deref().clone();
            let value = event
                .target()
                .unwrap()
                .unchecked_into::<HtmlInputElement>()
                .checked();

            data.selected = value;
            portfolio.set(data);
        })
    };

    let submit_form = |new: bool| {
        let portfolio = portfolio.clone();
        let message = message.clone();
        let onclose = props.onclose.clone();

        Callback::from(move |_| {
            let dispatch = dispatch.clone();
            let portfolio = portfolio.clone();
            let message = message.clone();
            let onclose = onclose.clone();

            spawn_local(async move {
                #[derive(Serialize)]
                struct Args {
                    portfolio: Portfolio,
                }

                dispatch.reduce(|state| state.as_waiting().into());

                let portfolio = portfolio.deref().clone();
                let args = to_value(&Args {
                    portfolio: portfolio.clone(),
                })
                .unwrap();
                let result: Option<String> =
                    from_value(js::invoke("save_portfolio", args).await).unwrap();

                if let Some(msg) = result {
                    message.set(DialogMessage::Error(msg.into()));

                    dispatch.reduce(|state| state.as_ready().into());
                } else if new {
                    dispatch.reduce(|state| {
                        state
                            .map(|data| store::Data {
                                page_message: store::PageMessage::Success(
                                    format!(
                                        "Portfolio \"{}\" created successfully.",
                                        portfolio.name
                                    )
                                    .into(),
                                ),
                                ..data.clone()
                            })
                            .into()
                    });

                    onclose.emit(MouseEvent::new("").unwrap());
                } else {
                    message.set(DialogMessage::Success("Successfully updated.".into()));
                }
            });
        })
    };

    let title = if props.portfolio.is_some() {
        "Existing Portfolio".to_string()
    } else {
        "New Portfolio".to_string()
    };

    let buttons = DialogButtons::Inclusive(if props.portfolio.is_some() {
        html! {
            <button type="button" class="btn btn-success" onclick={submit_form(false)} disabled={props.disabled}>
                <i class="fa-solid fa-floppy-disk"></i>
                <span>{"Update"}</span>
            </button>
        }
    } else {
        html! {
            <button type="button" class="btn btn-success" onclick={submit_form(true)} disabled={props.disabled}>
                <i class="fa-solid fa-floppy-disk"></i>
                <span>{"Create"}</span>
            </button>
        }
    });

    let message = message.deref().clone();

    html! {
        <Dialog onclose={props.onclose.clone()} {buttons} {title} {message} disabled={props.disabled}>
            <form>
                <fieldset disabled={props.disabled}>
                    <div class="form-floating mb-3">
                        <input type="text" class="form-control" id="portfolio_name" placeholder="Name" value={portfolio.name.clone()} oninput={handle_name_change} />
                        <label for="portfolio_name">{"Name"}</label>
                    </div>
                    <div class="mb-3 form-check">
                        <input type="checkbox" class="form-check-input" id="portfolio_selected" checked={portfolio.selected} oninput={handle_selected_change} />
                        <label class="form-check-label" for="portfolio_selected">{"Current portfolio"}</label>
                    </div>
                </fieldset>
            </form>
        </Dialog>
    }
}
