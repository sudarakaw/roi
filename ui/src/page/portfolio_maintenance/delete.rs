/* ui/src/page/portfolio_maintenance/delete.rs: Portfolio remove dialog
 *
 * Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

use roi::db::Portfolio;
use serde::Serialize;
use serde_wasm_bindgen::{from_value, to_value};
use wasm_bindgen_futures::spawn_local;
use web_sys::MouseEvent;
use yew::{function_component, html, Callback, Html, Properties};
use yewdux::prelude::use_store;

use crate::{
    component::{Dialog, DialogButtons},
    js,
    store::{self, State},
};

#[derive(Properties, PartialEq)]
pub struct DeleteConfirmProps {
    pub portfolio: Portfolio,
    pub disabled: bool,

    pub onclose: Callback<MouseEvent>,
}

#[function_component]
pub fn DeleteConfirm(props: &DeleteConfirmProps) -> Html {
    let (_, dispatch) = use_store::<State>();

    let title = "Remove Portfolio".to_string();

    let handle_delete_confirm = {
        let portfolio = props.portfolio.clone();
        let onclose = props.onclose.clone();

        Callback::from(move |_| {
            let dispatch = dispatch.clone();
            let portfolio = portfolio.clone();
            let onclose = onclose.clone();

            spawn_local(async move {
                #[derive(Serialize)]
                struct Args {
                    id: String,
                }

                dispatch.reduce(|state| state.as_waiting().into());

                let args = to_value(&Args {
                    id: portfolio.id.into(),
                })
                .unwrap();

                let result: Option<String> =
                    from_value(js::invoke("delete_portfolio", args).await).unwrap();

                if let Some(msg) = result {
                    dispatch.reduce(|state| {
                        state
                            .map(|data| store::Data {
                                page_message: store::PageMessage::Error(msg.clone().into()),
                                ..data.clone()
                            })
                            .as_ready()
                            .into()
                    });
                } else {
                    dispatch.reduce(|state| {
                        state
                            .map(|data| store::Data {
                                page_message: store::PageMessage::Success(
                                    format!(
                                        "Portfolio \"{}\" was successfully removed.",
                                        portfolio.name
                                    )
                                    .into(),
                                ),
                                ..data.clone()
                            })
                            .into()
                    });
                }

                onclose.emit(MouseEvent::new("").unwrap());
            });
        })
    };

    let buttons = DialogButtons::Exclusive(html! {
        <>
            <button
                type="button"
                class="btn btn-danger"
                disabled={props.disabled}
                onclick={handle_delete_confirm}>
                <i class="fa-solid fa-thumbs-up"></i>
                <span>{"Yes"}</span>
            </button>
            <button
                type="button"
                class="btn btn-success"
                disabled={props.disabled}
                onclick={props.onclose.clone()}>
                <i class="fa-solid fa-thumbs-down"></i>
                <span>{"No"}</span>
            </button>
        </>
    });

    html! {
        <Dialog onclose={props.onclose.clone()} {buttons} {title} disabled={props.disabled} >
            <p class="lead">
                <span>{"Are you sure you want to remove portfolio "}</span>
                <strong>{"\""}{props.portfolio.name.clone()}{"\""}</strong>
                <span>{"?"}</span>
            </p>
        </Dialog>
    }
}
