/* ui/src/page/summary/mod.rs: Summary page
 *
 * Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

use std::{collections::HashMap, ops::Deref};

use chrono::{Datelike, Duration, Local, Months, NaiveDate};
use roi::db::Maturity;
use web_sys::MouseEvent;
use yew::{classes, function_component, html, use_state, Callback, Html, Properties};
use yew_router::prelude::Link;
use yewdux::prelude::use_store;

use crate::{
    component::{Dialog, DialogButtons, Message, MessageType},
    page::{PageDialog, Route},
    store::{State, WindowSize},
    util::{number_format, number_suffix},
};

const CELL_WIDTH: u8 = 130;

fn months_fitting(window_width: u32) -> Vec<NaiveDate> {
    let today = Local::now().date_naive();
    let mut count = window_width.saturating_sub(300) / CELL_WIDTH as u32;

    if 12 < count {
        count = 12;
    }

    (0..count)
        .map(|i| today.checked_add_months(Months::new(i)).unwrap())
        .collect()
}

#[function_component]
pub fn Summary() -> Html {
    let dialog = use_state(|| PageDialog::<Maturity>::None);
    let (state, _) = use_store::<State>();
    let data = state.deref().data();
    let mut accounts = data
        .map(|d| d.accounts.iter().filter(|a| !a.archived).collect())
        .unwrap_or_else(Vec::new);

    if accounts.is_empty() {
        html! {
            <div class="container mt-5">
                <Message r#type={MessageType::Warning}>
                    <div class="d-flex gap-2 justify-content-center align-items-center">
                        <span>{"No accounts found in current portfolio. Click here to "}</span>
                        <Link<Route>
                            to={Route::AccountMaintenance}
                            classes={classes!("btn", "btn-sm", "btn-warning")}>
                            {"Add Accounts"}
                        </Link<Route>>
                    </div>
                </Message>
            </div>
        }
    } else {
        let window_width = data
            .map(|d| d.window_size.width)
            .unwrap_or_else(|| WindowSize::default().width);
        let months = months_fitting(window_width);

        let transaction_map = data
            .map(|d| d.transactions.clone())
            .unwrap_or_else(HashMap::new);

        let max_date = months
            .clone()
            .into_iter()
            .max()
            .and_then(|d| NaiveDate::from_ymd_opt(d.year(), d.month(), 1))
            .and_then(|d| d.checked_add_months(Months::new(1)))
            .unwrap_or(Local::now().date_naive());

        accounts.sort_by(|a, b| {
            let result = a.account_type.cmp(&b.account_type);

            if result.is_eq() {
                return a.number.cmp(&b.number);
            }

            result
        });

        let monthly_returns = state.get_monthly_return(months.clone());

        let (total_investment, total_rate) = accounts
            .clone()
            .into_iter()
            .map(|a| {
                transaction_map
                    .get(&a.get_id())
                    .map(|t| (t.amount, t.rate))
                    .unwrap_or((0, 0))
            })
            .fold((0, 0), |(acc_amount, acc_rate), (amount, rate)| {
                (acc_amount + amount, acc_rate + rate)
            });

        let twelve_month_returns = state.get_monthly_return(
            (0..12)
                .map(|i| {
                    Local::now()
                        .date_naive()
                        .checked_add_months(Months::new(i))
                        .unwrap()
                })
                .collect(),
        );
        let twelve_month_return_average = if twelve_month_returns.is_empty() {
            0
        } else {
            twelve_month_returns.values().sum::<i64>() / twelve_month_returns.len() as i64
        };

        let hide_dialog = {
            let dialog = dialog.clone();

            Callback::from(move |_| {
                dialog.set(PageDialog::None);
            })
        };

        let handle_cell_click = |maturity: Option<Maturity>| {
            let dialog = dialog.clone();

            Callback::from(move |_| {
                let maturity = maturity.clone();

                if let Some(m) = maturity {
                    dialog.set(PageDialog::Info(m));
                }
            })
        };

        let dialog = match dialog.deref() {
            PageDialog::Info(maturity) => {
                html! { <MaturityDialog maturity={ maturity.clone() } onclose={ hide_dialog } />}
            }
            _ => html! {},
        };

        html! {
            <>

            { dialog }

            <div class="container-fluid summary-container">
                <table class="table table-bordered">
                    <thead class="table-secondary">
                        <tr>
                            <th>{"Account"}</th>
                            <th class="text-end">{"Investment"}</th>
                            {for
                                months
                                    .clone()
                                    .into_iter()
                                    .map(|month| html! { <CellMonth {month} /> })
                            }
                        </tr>

                        <tr class="table-warning row-total">
                            <th>{"Total"}</th>
                            <th class="month text-end">
                                <small class="d-block text-secondary">{number_format((total_rate / accounts.len() as u16).into(), ",")}{"%"}</small>
                                <span class="font-monospace">{number_format(total_investment, ",")}</span>
                            </th>
                            {for
                                monthly_returns
                                    .values()
                                    .map(|total| html! {
                                        <th class="month text-end">
                                            <small class="d-block">{'\u{00a0}'}</small>
                                            <span class="font-monospace">{number_format(*total, ",")}</span>
                                        </th>
                                    })
                            }
                        </tr>

                        <tr class="table-info row-monthly-return">
                            <th>{"Monthly Average"}</th>
                            <th class="month text-end">
                                <small class="d-block">{'\u{00a0}'}</small>
                                <span class="font-monospace">{number_format(twelve_month_return_average, ",")}</span>
                            </th>
                            {for
                                monthly_returns
                                    .iter()
                                    .map(|_| html!{ <th class="month text-end"></th> })
                            }
                        </tr>
                    </thead>
                    <tbody>
                    {
                        for accounts.into_iter().map(|account| {
                            let transaction = transaction_map
                                .get(&account.get_id());
                            let amount_text = transaction
                                .map(|t| number_format(t.amount, ","))
                                .unwrap_or('\u{00a0}'.to_string());
                            let rate_text = transaction
                                .map(|t| format!("{}%", number_format(t.rate.into(), ",")))
                                .unwrap_or("".to_string());
                            let maturities = transaction
                                .map(|t| t.get_maturities(&max_date, account.days_per_year))
                                .unwrap_or_default();

                            html! {
                                <tr>
                                    <td class="account-cell position-relative">
                                        <div>
                                            <small>{account.account_type.to_string()}</small>
                                            <strong class="text-dark">{account.number.clone()}</strong>
                                        </div>
                                    </td>
                                    <td class="transaction-cell position-relative text-end">
                                        <div>
                                            <small class="text-secondary">{rate_text}</small>
                                            <span class="font-monospace">{amount_text}</span>
                                        </div>

                                        <div class="buttons d-none position-absolute top-0 end-0" title={"Transaction Maintenance"}>
                                            <Link<Route>
                                                to={Route::TransactionMaintenance { accountid: account.get_id() }}
                                                classes={classes!("btn", "btn-primary")}>
                                                <i class="fa-solid fa-money-bill-transfer"></i>
                                            </Link<Route>>
                                        </div>
                                    </td>
                                    {for
                                        months
                                            .clone()
                                            .into_iter()
                                            .map(|month| maturities.clone().into_iter().find(|m| m.date().with_day(1) == month.with_day(1) ))
                                            .map(|maturity| {
                                                html! { <CellTransaction maturity={ maturity.clone() } onbreakdown={ handle_cell_click(maturity) } />  }
                                            })
                                    }
                                </tr>
                            }
                        })
                    }
                    </tbody>
                </table>
            </div>

            </>
        }
    }
}

#[derive(Properties, PartialEq)]
struct CellMonthProps {
    month: NaiveDate,
}

#[function_component]
fn CellMonth(props: &CellMonthProps) -> Html {
    html! {
        <th class="text-end">{props.month.format("%B / %y").to_string()}</th>
    }
}

#[derive(Properties, PartialEq)]
struct CellTransactionProps {
    maturity: Option<Maturity>,

    onbreakdown: Callback<MouseEvent>,
}

#[function_component]
fn CellTransaction(props: &CellTransactionProps) -> Html {
    let mut date_text_class = classes!("d-block", "mb-2");

    let date = props.maturity.clone();
    let date_diff = date
        .clone()
        .map(|d| d.date() - Local::now().date_naive())
        .unwrap_or(Duration::days(0));
    let date_text = date
        .clone()
        .map(|d| {
            if 0 < date_diff.num_days() && 16 > date_diff.num_days() {
                let weeks = date_diff.num_days() / 8;

                date_text_class.push(format!("within-weeks week{}", weeks));

                html! { format!("in {} days", date_diff.num_days()) }
            } else {
                date_text_class.push("normal-date");

                if 0 == date_diff.num_days() {
                    date_text_class.push("today");
                } else if 0 > date_diff.num_days() {
                    date_text_class.push("done");
                }

                html! {
                    <>
                        { d.date().format("%A, %d").to_string() }
                        <sup>
                            {
                                date
                                    .clone()
                                    .map(|d| number_suffix(d.date().day()))
                                    .unwrap_or("")
                            }
                        </sup>
                    </>
                }
            }
        })
        .unwrap_or(html! { '\u{00a0}' });

    let date_title = date
        .clone()
        .map(|d| {
            format!(
                "{}{}",
                d.date().format("%A, %d"),
                date.clone()
                    .map(|d| number_suffix(d.date().day()))
                    .unwrap_or("")
            )
        })
        .unwrap_or("".to_string());

    let amount_text = props
        .maturity
        .clone()
        .map(|m| number_format(m.amount_paid(), ","))
        .unwrap_or('\u{00a0}'.to_string());

    let cell_class = classes!("text-end", date.map(|_| "have-maturity").unwrap_or(""));

    html! {
        <td class={ cell_class } style={format!("min-width: {}px", CELL_WIDTH)}>
            <small class={ date_text_class } title={ date_title }>{ date_text }</small>
            <span class="font-monospace" onclick={ props.onbreakdown.clone() }>{ amount_text }</span>
        </td>
    }
}

#[derive(Properties, PartialEq)]
pub struct MaturityProps {
    pub maturity: Maturity,

    pub onclose: Callback<MouseEvent>,
}

#[function_component]
fn MaturityDialog(props: &MaturityProps) -> Html {
    let buttons = DialogButtons::Exclusive(html! {});

    html! {
        <Dialog onclose={props.onclose.clone()} {buttons} title="Maturity Breakdown">
            <table class="table table-bordered border-secondary-subtle fs-5">
                <tbody>
                    <tr>
                        <th class="table-active" scope="row">{"Return on maturity"}</th>
                        <td class="text-end font-monospace">{ number_format(props.maturity.total_return(), ",") }</td>
                    </tr>
                    <tr>
                        <th class="table-active" scope="row">{"Withholding Tax"}</th>
                        <td class="text-end font-monospace">{ number_format(props.maturity.wht(), ",") }</td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <th class="table-active" scope="row">{"Amount received"}</th>
                        <th class="text-end font-monospace">{ number_format(props.maturity.amount_paid(), ",") }</th>
                    </tr>
                </tfoot>
            </table>
        </Dialog>
    }
}
