/* ui/src/page/mod.rs: Page & Route parent module
 *
 * Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

mod account_maintenance;
mod portfolio_maintenance;
mod summary;
mod transaction_maintenance;

use roi::db::AccountId;
use yew::{html, Html};
use yew_router::{prelude::Redirect, Routable};

use crate::page::transaction_maintenance::TransactionMaintenance;

use self::{
    account_maintenance::AccountMaintenance, portfolio_maintenance::PortfolioMaintenance,
    summary::Summary,
};

#[derive(Routable, Clone, PartialEq)]
pub enum Route {
    #[at("/")]
    Summary,
    #[at("/nuzsecckupkxhwkb")]
    PortfolioMaintenance,
    #[at("/fwvzogxerodmjarv")]
    AccountMaintenance,
    #[at("/idtpqxxnvjxxshss/:accountid")]
    TransactionMaintenance { accountid: AccountId },
    #[at("/zaexjdfywbbtxzue")]
    #[not_found]
    NotFound,
}

pub fn switch(route: Route) -> Html {
    match route {
        Route::Summary => html! { <Summary /> },
        Route::PortfolioMaintenance => html! { <PortfolioMaintenance /> },
        Route::AccountMaintenance => html! { <AccountMaintenance /> },
        Route::TransactionMaintenance { accountid } => {
            html! { <TransactionMaintenance {accountid} /> }
        }
        Route::NotFound => html! { <Redirect<Route> to={Route::Summary} /> },
    }
}

#[derive(Clone, PartialEq)]
enum PageDialog<T> {
    None,
    Form(Option<T>),
    DeleteConfirm(T),
    Info(T),
}
