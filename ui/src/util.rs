/* ui/src/util.rs: utility functions
 *
 * Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

use std::str::{from_utf8, FromStr};

pub fn number_format(value: i64, thousands_separator: &str) -> String {
    let int_part = value / 100;
    let float_part = value - (int_part * 100);

    // From https://stackoverflow.com/a/67834588
    let int_part = int_part
        .to_string()
        .as_bytes()
        .rchunks(3)
        .rev()
        .map(from_utf8)
        .collect::<Result<Vec<_>, _>>()
        .unwrap()
        .join(thousands_separator);

    format!("{}.{:02}", int_part, float_part)
}

pub fn from_formatted<T>(value: &str) -> T
where
    T: FromStr + Default,
{
    let mut value = value.to_string();

    if let Some(dot_pos) = value.find('.') {
        // Decimal point found in given string

        let l = value.len() - dot_pos;

        if 3 < l {
            // More than 2 decimal digits in string

            value = value[..(value.len() - (l - 3))].to_string();
        } else {
            // Less than 2 decial digits in string

            value.push_str(&"0".repeat(3 - l));
        }
    } else {
        // No decimal point found in given string

        value.push_str(&"0".repeat(2));
    }

    value
        .replace(['.', ','], "")
        .parse::<T>()
        .unwrap_or_else(|_| T::default())
}

pub fn number_suffix<'a>(num: u32) -> &'a str {
    match num % 10 {
        1 => "st",
        2 => "nd",
        3 => "rd",
        _ => "th",
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_number_format() {
        let test_data = vec![
            "0.02",
            "0.56",
            "6.54",
            "45.67",
            "765.43",
            "3,456.78",
            "87,654.32",
            "234,567.89",
            "9,876,543.21",
            "12,345,678.90",
        ];
        let test_data_with_separator = test_data
            .clone()
            .into_iter()
            .map(|v| {
                (
                    v,
                    (v.replace(',', "").parse::<f64>().unwrap() * 100.0) as i64,
                )
            })
            .collect::<Vec<(&str, i64)>>();
        let test_data_without_separator = test_data
            .into_iter()
            .map(|v| {
                (
                    v.replace(',', ""),
                    (v.replace(',', "").parse::<f64>().unwrap() * 100.0) as i64,
                )
            })
            .collect::<Vec<(String, i64)>>();

        for (expected, given) in test_data_with_separator {
            assert_eq!(number_format(given, ","), expected);
        }

        for (expected, given) in test_data_without_separator {
            assert_eq!(number_format(given, ""), expected);
        }
    }

    #[test]
    fn test_from_formatted() {
        let test_data = vec![
            ("0", 0),
            ("0.0", 0),
            ("0.00", 0),
            ("1", 100),
            ("1.", 100),
            (".1", 10),
            ("0.1", 10),
            ("0.10", 10),
            ("0.12", 12),
            ("0.120", 12),
            ("0.123", 12),
            ("1.2", 120),
            ("1.20", 120),
            ("1.23", 123),
            ("1.234", 123),
        ];

        for (given, expected) in test_data {
            assert_eq!(from_formatted::<i64>(given), expected);
        }
    }
}
