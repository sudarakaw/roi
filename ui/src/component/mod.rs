/* ui/src/component/mod.rs: Shared UI components
 *
 * Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

mod message;
pub use message::{Message, MessageType};

mod header;
pub use header::Header;

mod dialog;
pub use dialog::{Dialog, DialogButtons, DialogMessage};

mod page_message;
pub use page_message::PageMessage;
