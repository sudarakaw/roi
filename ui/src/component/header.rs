/* ui/src/component/header.rs: Application header
 *
 * Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

use serde::Serialize;
use serde_wasm_bindgen::to_value;
use wasm_bindgen::JsCast;
use web_sys::{HtmlInputElement, InputEvent};
use yew::{classes, function_component, html, platform::spawn_local, Callback, Html, Properties};
use yew_router::prelude::{use_navigator, use_route, Link};
use yewdux::prelude::use_store;

use crate::{js, page::Route, store::State};

#[derive(Properties, PartialEq)]
pub struct HeaderProps {
    #[prop_or_default]
    pub disabled: bool,
}

#[function_component]
pub fn Header(props: &HeaderProps) -> Html {
    let (state, dispatch) = use_store::<State>();
    let nav = use_navigator().unwrap();
    let portfolios = state.data().map(|d| d.portfolios.clone()).unwrap_or(vec![]);
    let current_route = use_route::<Route>();

    let portfolio_change_handler = Callback::from(move |event: InputEvent| {
        let dispatch = dispatch.clone();
        let nav = nav.clone();
        let id = event
            .target()
            .unwrap()
            .unchecked_into::<HtmlInputElement>()
            .value();

        spawn_local(async move {
            #[derive(Serialize)]
            struct Args<'a> {
                id: &'a str,
            }

            dispatch.reduce(|state| state.as_waiting().into());

            let args = to_value(&Args { id: &id }).unwrap();

            nav.push(&Route::Summary);

            js::invoke("change_portfolio", args).await;
        });
    });

    html! {
        <>

        <header class="app-header py-2">
            <div class="container d-flex flex-wrap justify-content-around">
                <div class="header-logo d-flex align-items-center link-body-emphasis">
                    <img class="bi me-2" src="/assets/img/icons/32x32.png" alt="ROI" />
                    <strong class="d-none d-sm-inline">{"Return On Investment"}</strong>
                    <strong class="d-inline d-sm-none">{"ROI"}</strong>
                </div>

                <div class="col-6 col-sm-auto row g-3 align-items-center">
                    <div class="input-group">
                        <label class={format!("portfolio-selector-label input-group-text px-1 ps-sm-3 ps-md-1 ps-lg-3 border-dark bg-dark {}", if props.disabled { "text-secondary" } else {"text-light"})} for="portfolioSelector">
                            <span class="d-none d-sm-inline d-md-none d-lg-inline">{"Portfolio: "}</span>
                        </label>
                        <select
                            class={format!("portfolio-selector form-select border-dark bg-dark{}", if props.disabled { " text-secondary" } else {""})}
                            disabled={props.disabled}
                            oninput={portfolio_change_handler}>
                        {
                            portfolios.iter().map(|p| {
                                let id_string:String = p.id.clone().into();

                                html! {
                                    <option value={id_string} selected={p.selected}>{p.name.clone()}</option>
                                }
                            }).collect::<Html>()
                        }
                        </select>
                        <Link<Route>
                            to={Route::PortfolioMaintenance}
                            classes={classes!("portfolio-maintenance", "btn", "btn-dark")}>
                            <i class="fa-solid fa-gear"></i>
                        </Link<Route>>
                    </div>
                </div>
            </div>
        </header>

        <nav class="navbar app-nav">
            <div class="container justify-content-center">
                <ul class="nav">
                    <li class="nav-item">
                        <Link<Route>
                            to={Route::Summary}
                            classes={classes!("btn", "btn-light", if current_route == Some(Route::Summary)  { "active"} else { "" })}>
                            {"SUMMARY"}
                        </Link<Route>>
                        <Link<Route>
                            to={Route::AccountMaintenance}
                            classes={classes!("btn", "btn-light", if current_route == Some(Route::AccountMaintenance)  { "active"} else { "" })}>
                            {"ACCOUNTS"}
                        </Link<Route>>
                    </li>
                </ul>
            </div>
        </nav>

        </>
    }
}
