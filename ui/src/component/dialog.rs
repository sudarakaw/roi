/* ui/src/component/dialog.rs: Dialog box UI
 *
 * Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

use web_sys::MouseEvent;
use yew::{function_component, html, Callback, Children, Html, Properties};

use super::{Message, MessageType};

#[derive(Clone, PartialEq)]
pub enum DialogButtons {
    Inclusive(Html),
    Exclusive(Html),
}

impl Default for DialogButtons {
    fn default() -> Self {
        Self::Inclusive(html! {})
    }
}

#[derive(Clone, Debug, Default, PartialEq)]
pub enum DialogMessage {
    #[default]
    None,
    Error(Html),
    Success(Html),
}

impl From<DialogMessage> for Html {
    fn from(value: DialogMessage) -> Self {
        match value {
            DialogMessage::None => html! {},
            DialogMessage::Error(msg) => html! {
                <div class="modal-footer">
                    <Message r#type={MessageType::Error}>
                        <div class="text-center">{msg}</div>
                    </Message>
                </div>
            },
            DialogMessage::Success(msg) => html! {
                <div class="modal-footer">
                    <Message r#type={MessageType::Success}>
                        <div class="text-center">{msg}</div>
                    </Message>
                </div>
            },
        }
    }
}

#[derive(Properties, PartialEq)]
pub struct Props {
    #[prop_or_default]
    pub title: String,

    #[prop_or_default]
    pub buttons: DialogButtons,

    #[prop_or_default]
    pub message: DialogMessage,

    #[prop_or_default]
    pub onclose: Callback<MouseEvent>,

    #[prop_or_default]
    pub children: Children,

    #[prop_or_default]
    pub disabled: bool,
}

#[function_component]
pub fn Dialog(props: &Props) -> Html {
    html! {
        <>
        <div class="modal fade show d-block" tabindex="-1">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{props.title.clone()}</h5>
                        <button type="button" class="btn-close" onclick={props.onclose.clone()} disabled={props.disabled}></button>
                    </div>
                    <div class="modal-body">{ for props.children.iter() }</div>
                    <div class="modal-footer">
                    {
                        match props.buttons.clone() {
                            DialogButtons::Inclusive(html) => html! {
                                <>
                                    <button
                                        type="button"
                                        class="btn btn-light"
                                        onclick={props.onclose.clone()}
                                        disabled={props.disabled}>
                                        {"Close"}
                                    </button>
                                    {html}
                                </>
                            },
                            DialogButtons::Exclusive(html) => html,
                        }
                    }
                    </div>
                    { props.message.clone() }
                </div>
            </div>
        </div>
        <div class="modal-backdrop fade show"></div>
        </>
    }
}
