/* ui/src/component/message.rs: Message box based on Bootstrap alert
 *
 * Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

use std::fmt;

use yew::{function_component, html, Children, Html, Properties};

#[derive(PartialEq)]
pub enum MessageType {
    Error,
    Info,
    Success,
    Warning,
}

impl Default for MessageType {
    fn default() -> Self {
        Self::Info
    }
}

impl fmt::Display for MessageType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Error => write!(f, "danger"),
            Self::Info => write!(f, "info"),
            Self::Success => write!(f, "success"),
            Self::Warning => write!(f, "warning"),
        }
    }
}

#[derive(Properties, PartialEq)]
pub struct MessageProps {
    #[prop_or_default]
    pub children: Children,

    #[prop_or_default]
    pub r#type: MessageType,

    #[prop_or_default]
    pub icon: Option<String>,
}

#[function_component]
pub fn Message(props: &MessageProps) -> Html {
    let classes = vec![
        "alert".to_string(),
        format!("alert-{}", props.r#type),
        "flex-grow-1".to_string(),
        "d-flex".to_string(),
        "align-items-center".to_string(),
    ];

    let icon = if let Some(icon) = props.icon.clone() {
        let classes = vec![icon, "me-2".to_string(), "flex-shrink-0".to_string()];

        html! { <i class={classes}></i>}
    } else {
        html! {}
    };

    html! {
        <div class={classes} role="alert">
            { icon }
            <div class="col-12">{ for props.children.iter() }</div>
        </div>
    }
}
