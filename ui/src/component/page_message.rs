/* ui/src/component/page_message.rs: Message that show under the application header
 *
 * Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

use gloo::timers::callback::Timeout;
use wasm_bindgen::JsValue;
use yew::{function_component, html, use_effect, use_state, Html};
use yewdux::prelude::use_store;

use crate::{
    component::{Message, MessageType},
    store::{self, State},
};

#[function_component]
pub fn PageMessage() -> Html {
    let timer = use_state(|| None::<JsValue>);
    let (state, dispatch) = use_store::<State>();
    let page_message = state
        .data()
        .map(|data| data.page_message.clone())
        .unwrap_or_else(|| store::PageMessage::None);
    let message_type: MessageType = page_message.clone().into();

    let icon = match &page_message {
        store::PageMessage::None => None,
        store::PageMessage::Success(_) => Some("fa-regular fa-circle-check fs-3"),
        store::PageMessage::Error(_) => Some("fa-regular fa-circle-exclamation fs-3"),
    };

    let close_delay = match &page_message {
        store::PageMessage::Error(_) => 18000,
        _ => 13000,
    };

    use_effect(move || {
        if timer.is_none() {
            let id = {
                let timer = timer.clone();

                Timeout::new(close_delay, move || {
                    dispatch.reduce(|state| {
                        state
                            .map(|data| store::Data {
                                page_message: store::PageMessage::None,
                                ..data.clone()
                            })
                            .into()
                    });

                    timer.set(None);
                })
                .forget()
            };

            timer.set(Some(id));
        }
    });

    match page_message {
        store::PageMessage::None => html! {},
        store::PageMessage::Success(inner_html) => html! {
            <div class="container page-message mt-4">
                <Message r#type={message_type} {icon}>
                    <div class="fs-5">{inner_html}</div>
                </Message>
            </div>
        },
        store::PageMessage::Error(inner_html) => html! {
            <div class="container page-message mt-4">
                <Message r#type={message_type} {icon}>
                    <div class="fs-5">{inner_html}</div>
                </Message>
            </div>
        },
    }
}
