/* ui/src/store.rs: Yewdux Store to share runtime data across components/pages.
 *
 * Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

use std::collections::{BTreeMap, HashMap};

use chrono::{Datelike, NaiveDate};
use roi::db::{Account, AccountId, Portfolio, Transaction};
use serde::Deserialize;
use serde_wasm_bindgen::from_value;
use web_sys::window;
use yew::Html;
use yewdux::store::Store;

use crate::component::MessageType;

#[derive(Debug, Clone, Default, PartialEq)]
pub enum PageMessage {
    #[default]
    None,
    Success(Html),
    Error(Html),
}

impl From<PageMessage> for MessageType {
    fn from(value: PageMessage) -> Self {
        match value {
            PageMessage::None => Self::Info,
            PageMessage::Success(_) => Self::Success,
            PageMessage::Error(_) => Self::Error,
        }
    }
}

#[derive(Clone, Debug, Deserialize, PartialEq)]
pub struct WindowSize {
    pub height: u32,
    pub width: u32,
}

impl Default for WindowSize {
    fn default() -> Self {
        let width = from_value(window().and_then(|w| w.inner_width().ok()).into()).unwrap_or(800);

        Self { height: 600, width }
    }
}

#[derive(Clone, Default, Debug, PartialEq)]
pub struct Data {
    pub portfolios: Vec<Portfolio>,
    pub accounts: Vec<Account>,
    pub transactions: HashMap<AccountId, Transaction>,
    pub page_message: PageMessage,
    pub window_size: WindowSize,
}

impl Data {
    fn get_account_by_id(&self, account_id: &AccountId) -> Option<Account> {
        self.accounts
            .clone()
            .into_iter()
            .find(|acc| acc.get_id() == *account_id)
    }
}

#[derive(Debug, Store, Clone, PartialEq, Default)]
pub enum State {
    #[default]
    Loading,
    LoadingFailed(String),
    Ready(Data),
    Waiting(Data),
}

impl State {
    pub fn data(&self) -> Option<&Data> {
        match self {
            Self::Loading => None,
            Self::LoadingFailed(_) => None,
            Self::Ready(data) => Some(data),
            Self::Waiting(data) => Some(data),
        }
    }

    pub fn map<F>(&self, f: F) -> Self
    where
        F: Fn(&Data) -> Data,
    {
        match self {
            Self::Loading => self.clone(),
            Self::LoadingFailed(_) => self.clone(),
            Self::Ready(data) => Self::Ready(f(data)),
            Self::Waiting(data) => Self::Waiting(f(data)),
        }
    }

    pub fn as_ready(&self) -> Self {
        match self {
            Self::Loading => Self::Ready(Data::default()),
            Self::LoadingFailed(_) => self.clone(),
            Self::Ready(_) => self.clone(),
            Self::Waiting(data) => Self::Ready(data.clone()),
        }
    }

    pub fn as_waiting(&self) -> Self {
        match self {
            Self::Loading => self.clone(),
            Self::LoadingFailed(_) => self.clone(),
            Self::Ready(data) => Self::Waiting(data.clone()),
            Self::Waiting(_) => self.clone(),
        }
    }

    pub fn selected_portfolio(&self) -> Option<Portfolio> {
        self.data()
            .and_then(|data| data.portfolios.iter().find(|p| p.selected))
            .cloned()
    }

    pub fn get_monthly_return(&self, target_months: Vec<NaiveDate>) -> BTreeMap<NaiveDate, i64> {
        let mut monthly_return = BTreeMap::new();

        if let Self::Ready(data) = self {
            for target_month in target_months {
                let year = target_month.year();
                let month = target_month.month();

                let last_date = NaiveDate::from_ymd_opt(year, month + 1, 1)
                    .or_else(|| NaiveDate::from_ymd_opt(year + 1, 1, 1))
                    .and_then(|d| d.pred_opt());

                if let Some(last_date) = last_date {
                    // Get transactions for all available accounts
                    for transaction in data.transactions.values() {
                        // Get account for this transaction
                        let account = match data.get_account_by_id(&transaction.get_account_id()) {
                            None => {
                                // Unable to calculate maturities without
                                // account. Skip this iteration.
                                continue;
                            }
                            Some(acc) => acc,
                        };

                        // For each transaction get maturities up-to end of
                        // given month.
                        for m in transaction.get_maturities(&last_date, account.days_per_year) {
                            // 1. Filter out maturities that occur out of given
                            //    month.
                            if last_date
                                .with_day(1)
                                .map(|d| d <= m.date())
                                .unwrap_or(false)
                                && last_date >= m.date()
                            {
                                let amount = m.amount_paid();

                                // 2. Extract amount paid from the maturity.
                                monthly_return
                                    .entry(target_month)
                                    .and_modify(|v| *v += amount)
                                    .or_insert(amount);
                            }
                        }
                    }
                }
            }
        }

        monthly_return
    }
}
