const { getCurrentWindow } = window.__TAURI__.window

export async function onResized(handler) {
  const window = getCurrentWindow()

  window.onResized(handler).await
}
