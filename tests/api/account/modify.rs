/* tests/api/account/modify.rs
 *
 * Copyright 2022 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

use roi::db::{Account, Portfolio};

use crate::helper::{fake_account, get_db_pool};

#[tokio::test]
async fn number_must_not_be_empty() {
    // Setup
    let db_pool = get_db_pool().await;
    let portfolio = Portfolio::get_selected(&db_pool).await.unwrap();

    let mut account = fake_account(portfolio.id);

    let result = account.save(&db_pool).await;
    assert!(result.is_ok());

    account.number = "".to_string();

    // Action
    let result = account.save(&db_pool).await;
    assert!(result.is_err());
    if let Err(msg) = result {
        assert_eq!("Account number must not be empty.", msg.to_string());
    }
}

#[tokio::test]
async fn multiple_saves_does_no_add_multiple_records_to_database() {
    // Setup
    let db_pool = get_db_pool().await;
    let portfolio = Portfolio::get_selected(&db_pool).await.unwrap();
    let initial_record_count = Account::get_all(&db_pool, &portfolio.id).await.len();

    assert_eq!(0, initial_record_count);

    let mut account = fake_account(portfolio.id.clone());

    account.save(&db_pool).await.unwrap();
    let count_after_first_save = Account::get_all(&db_pool, &portfolio.id).await.len();

    assert_eq!(1, count_after_first_save);

    account.save(&db_pool).await.unwrap();
    let count_after_second_save = Account::get_all(&db_pool, &portfolio.id).await.len();

    assert_eq!(1, count_after_second_save);
}
