/* tests/api/account/create.rs
 *
 * Copyright 2022 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

use fake::{
    faker::{name::en::Name, number::en::NumberWithFormat},
    Fake,
};
use roi::db::{Account, AccountType, Portfolio};

use crate::helper::{fake_account, get_db_pool};

#[tokio::test]
async fn number_must_not_be_empty() {
    // Setup
    let db_pool = get_db_pool().await;
    let portfolio = Portfolio::get_selected(&db_pool).await.unwrap();

    let mut account = fake_account(portfolio.id);
    account.number = "".to_string();

    // Action
    let result = account.save(&db_pool).await;
    assert!(result.is_err());
    if let Err(msg) = result {
        assert_eq!("Account number must not be empty.", msg.to_string());
    }
}

#[tokio::test]
async fn cannot_be_saved_with_invalid_portfolio() {
    // Setup
    let db_pool = get_db_pool().await;
    let invalid_portfolio = Portfolio::new(Name().fake(), false);

    // Action
    let mut account = Account::new(
        invalid_portfolio.id,
        AccountType::Savings,
        NumberWithFormat("").fake(),
        false,
        (364..=366).fake(),
    );

    let result = account.save(&db_pool).await;

    assert!(result.is_err());
    if let Err(msg) = result {
        assert_eq!(
            "Attempt to create account for invalid portfolio.",
            msg.to_string()
        );
    }
}

#[tokio::test]
async fn assigned_portfolio_must_exists() {
    // Setup
    let db_pool = get_db_pool().await;
    let mut invalid_portfolio = Portfolio::new(Name().fake(), false);
    let valid_portfolio = Portfolio::get_selected(&db_pool).await.unwrap();

    invalid_portfolio.save(&db_pool).await.unwrap();

    let mut invalid_portfolio_account = fake_account(invalid_portfolio.id.clone());
    let mut valid_portfolio_account = fake_account(valid_portfolio.id);

    invalid_portfolio.remove(&db_pool).await.unwrap();

    // Action
    let result = invalid_portfolio_account.save(&db_pool).await;
    assert!(result.is_err());

    let result = valid_portfolio_account.save(&db_pool).await;
    assert!(result.is_ok());
}
