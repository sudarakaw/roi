/* tests/api/portfolio/create.rs
 *
 * Copyright 2022 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

use fake::{faker::name::en::Name, Fake};
use roi::db::Portfolio;

use crate::helper::get_db_pool;

#[tokio::test]
async fn name_must_not_be_empty() {
    // Setup
    let db_pool = get_db_pool().await;

    let mut portfolio = Portfolio::new("".to_string(), false);

    // Action
    let result = portfolio.save(&db_pool).await;
    assert!(result.is_err());
    if let Err(msg) = result {
        assert_eq!(
            "Portfolio name must be non-empty & unique.",
            msg.to_string()
        );
    }
}

#[tokio::test]
async fn must_have_a_unique_name() {
    // Setup
    let db_pool = get_db_pool().await;
    let name: String = Name().fake();

    let mut unique_portfolio1 = Portfolio::new(name.clone(), false);
    let mut unique_portfolio2 = Portfolio::new(Name().fake(), false);
    let mut duplicate_portfolio = Portfolio::new(name.clone(), false);

    // Action
    let result = unique_portfolio1.save(&db_pool).await;
    assert!(result.is_ok());

    let result = unique_portfolio2.save(&db_pool).await;
    assert!(result.is_ok());

    let result = duplicate_portfolio.save(&db_pool).await;
    assert!(result.is_err());
    if let Err(msg) = result {
        assert_eq!(
            format!("Portfolio with name \"{}\" already exists.", name),
            msg.to_string()
        );
    }
}

#[tokio::test]
async fn must_maintain_single_selectable_policy() {
    // Setup
    let db_pool = get_db_pool().await;

    // Verify initial portfolio record
    let selected_portfolio = Portfolio::get_selected(&db_pool).await;
    assert!(selected_portfolio.is_ok());
    let selected_portfolio = selected_portfolio.unwrap();
    assert_eq!(selected_portfolio.name, "My Portfolio");

    // Add new **unselected** portfolio
    let new_name: String = Name().fake();
    let mut new_portfolio = Portfolio::new(new_name.clone(), false);
    new_portfolio.save(&db_pool).await.unwrap();
    let selected_portfolio = Portfolio::get_selected(&db_pool).await;
    assert!(selected_portfolio.is_ok());
    let selected_portfolio = selected_portfolio.unwrap();
    assert_ne!(new_name, selected_portfolio.name);

    // Add new **selected** portfolio
    let mut new_portfolio = Portfolio::new(Name().fake(), true);
    new_portfolio.save(&db_pool).await.unwrap();
    let selected_portfolio = Portfolio::get_selected(&db_pool).await;
    assert!(selected_portfolio.is_ok());
    let selected_portfolio = selected_portfolio.unwrap();
    assert_ne!(selected_portfolio.name, "My Portfolio");
}
