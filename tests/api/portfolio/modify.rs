/* tests/api/portfolio/modify.rs
 *
 * Copyright 2022 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

use fake::{faker::name::en::Name, Fake};
use roi::db::Portfolio;

use crate::helper::get_db_pool;

#[tokio::test]
async fn name_must_not_be_empty() {
    // Setup
    let db_pool = get_db_pool().await;

    let mut portfolio = Portfolio::new(Name().fake(), false);

    // Action
    let result = portfolio.save(&db_pool).await;
    assert!(result.is_ok());

    portfolio.name = "".to_string();

    let result = portfolio.save(&db_pool).await;
    assert!(result.is_err());
    if let Err(msg) = result {
        assert_eq!(
            "Portfolio name must be non-empty & unique.",
            msg.to_string()
        );
    }
}

#[tokio::test]
async fn must_maintain_unique_name() {
    // Setup
    let db_pool = get_db_pool().await;
    let name: String = Name().fake();

    let mut unique_portfolio = Portfolio::new(name.clone(), false);
    let mut duplicate_portfolio = Portfolio::new(Name().fake(), false);

    // Action
    let result = unique_portfolio.save(&db_pool).await;
    assert!(result.is_ok());

    let result = duplicate_portfolio.save(&db_pool).await;
    assert!(result.is_ok());

    duplicate_portfolio.name = name.clone();

    let result = duplicate_portfolio.save(&db_pool).await;
    assert!(result.is_err());
    if let Err(msg) = result {
        assert_eq!(
            format!("Portfolio with name \"{}\" already exists.", name),
            msg.to_string()
        );
    }
}

#[tokio::test]
async fn multiple_saves_does_no_add_multiple_records_to_database() {
    // Setup
    let db_pool = get_db_pool().await;
    let initial_record_count = Portfolio::get_all(&db_pool).await.len();

    assert_eq!(1, initial_record_count);

    let mut portfolio = Portfolio::new(Name().fake(), false);

    portfolio.save(&db_pool).await.unwrap();
    let count_after_first_save = Portfolio::get_all(&db_pool).await.len();

    assert_eq!(2, count_after_first_save);

    portfolio.save(&db_pool).await.unwrap();
    let count_after_second_save = Portfolio::get_all(&db_pool).await.len();

    assert_eq!(2, count_after_second_save);
}

#[tokio::test]
async fn unselecting_currently_selected_portfolio_is_not_allowed() {
    // Setup
    let db_pool = get_db_pool().await;
    let mut portfolio = Portfolio::new(Name().fake(), true);

    portfolio.save(&db_pool).await.unwrap();

    portfolio.selected = false;
    let result = portfolio.save(&db_pool).await;

    // Setting selected = false should fail.
    assert!(result.is_err());

    let selected_portfolio = Portfolio::get_selected(&db_pool).await.unwrap();

    // Created portfolio record is still selected
    assert_eq!(selected_portfolio.id, portfolio.id);
}

#[tokio::test]
async fn must_maintain_single_selectable_policy() {
    // Setup
    let db_pool = get_db_pool().await;

    // Add new **unselected** portfolio
    let mut new_portfolio = Portfolio::new(Name().fake(), false);
    let id = new_portfolio.save(&db_pool).await.unwrap();
    let mut existing_portfolio = Portfolio::get(&db_pool, id).await.unwrap();
    let selected_portfolio = Portfolio::get_selected(&db_pool).await.unwrap();
    assert_eq!(
        id, &existing_portfolio.id,
        "Comparing created vs retrieved records"
    );
    assert_ne!(
        &selected_portfolio.id, &existing_portfolio.id,
        "Comparing selected vs retrieved records"
    );

    // Modify existing **unselected** record to **selected** portfolio
    existing_portfolio.selected = true;
    existing_portfolio.save(&db_pool).await.unwrap();

    let previous_selection = Portfolio::get(&db_pool, &selected_portfolio.id)
        .await
        .unwrap();
    assert!(
        !previous_selection.selected,
        "Previous selection was not undone."
    );

    let selected_portfolio = Portfolio::get_selected(&db_pool).await;
    assert!(selected_portfolio.is_ok());
    let selected_portfolio = selected_portfolio.unwrap();
    assert_eq!(selected_portfolio.id, existing_portfolio.id);
    assert_ne!(selected_portfolio.id, previous_selection.id);
}
