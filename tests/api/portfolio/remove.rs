/* tests/api/portfolio/remove.rs
 *
 * Copyright 2022 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

use fake::{faker::name::en::Name, Fake};
use roi::db::Portfolio;

use crate::helper::get_db_pool;

#[tokio::test]
async fn removing_currently_selected_portfolio_is_not_allowed() {
    // Setup
    let db_pool = get_db_pool().await;
    let mut selected_portfolio = Portfolio::new(Name().fake(), true);
    let mut unselected_portfolio = Portfolio::new(Name().fake(), false);

    selected_portfolio.save(&db_pool).await.unwrap();
    unselected_portfolio.save(&db_pool).await.unwrap();

    // Attempt to remove the selected portfolio
    let result = selected_portfolio.remove(&db_pool).await;
    assert!(result.is_err());

    // Attempt to remove an unselected portfolio
    let result = unselected_portfolio.remove(&db_pool).await;
    assert!(result.is_ok());
}
