/* tests/api/helper.rs
 *
 * Copyright 2022 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

use chrono::{DateTime, Days, Local, Utc};
use fake::{
    faker::{chrono::en::DateTimeBefore, number::en::NumberWithFormat},
    Fake,
};
use roi::db::{Account, AccountId, AccountType, PortfolioId, Transaction};
use sqlx::SqlitePool;

pub async fn get_db_pool() -> SqlitePool {
    let db_pool = SqlitePool::connect("sqlite::memory:")
        .await
        .expect("Failed to connect to in-memory database");

    sqlx::migrate!()
        .run(&db_pool)
        .await
        .expect("Failed to apply database migrations");

    db_pool
}

pub fn fake_account(portfolio_id: PortfolioId) -> Account {
    Account::new(
        portfolio_id,
        AccountType::Savings,
        NumberWithFormat("#####-#####-#####").fake(),
        false,
        (364..=366).fake(),
    )
}

pub fn fake_transaction(account_id: AccountId) -> Transaction {
    let days_to_maturity = (1..9999).fake::<u16>();
    let invested_on = DateTimeBefore((Local::now() - Days::new(days_to_maturity.into())).into())
        .fake::<DateTime<Utc>>()
        .naive_local()
        .into();

    Transaction::new(
        account_id,
        (0..99999999999).fake(),
        (0..9999).fake(),
        (0..9999).fake(),
        invested_on,
        days_to_maturity,
    )
}
