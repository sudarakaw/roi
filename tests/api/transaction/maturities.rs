/* tests/api/transaction/maturities.rs
 *
 * Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

use chrono::NaiveDate;
use roi::db::{Portfolio, Transaction};

use crate::helper::{fake_account, fake_transaction, get_db_pool};

#[tokio::test]
async fn maturity_calculation_is_correct() {
    // Setup

    // 91 days
    const DAYS_TO_MATURE: u16 = 91;
    // [$|Rs.|...] 1,234,567.89
    const INVESTED_AMOUNT: i64 = 123456789;
    // 12.34% per annum
    const YEARLY_INTEREST: u16 = 1234;
    // 23.45% of maturity return
    const WHT_RATE: u16 = 2345;
    // Number of days per year to consider in retuen calculation
    const ANUAL_RETURN: i64 = INVESTED_AMOUNT
        // Interest is stored as cents (number of cents per 100 rupee)
        * (YEARLY_INTEREST as i64) / 100
        * (DAYS_TO_MATURE as i64)
        // Convert from cents
        / 100;

    let db_pool = get_db_pool().await;
    let portfolio = Portfolio::get_selected(&db_pool).await.unwrap();
    let mut account = fake_account(portfolio.id);

    account.save(&db_pool).await.unwrap();

    let mut transaction = fake_transaction(account.get_id());

    // There should be 12 monthly per year
    transaction.invested_on = NaiveDate::parse_from_str("2023-08-01", "%Y-%m-%d").unwrap();
    transaction.days_to_maturity = 30;

    let upto = NaiveDate::parse_from_str("2024-08-01", "%Y-%m-%d").unwrap();
    let maturities = transaction.get_maturities(&upto, account.days_per_year);

    assert_eq!(maturities.len(), 13);

    // There should be 4 quarterly maturities per year
    transaction.invested_on = NaiveDate::parse_from_str("2023-08-01", "%Y-%m-%d").unwrap();
    transaction.days_to_maturity = DAYS_TO_MATURE;
    transaction.amount = INVESTED_AMOUNT;
    transaction.rate = YEARLY_INTEREST;
    transaction.wht_rate = WHT_RATE;

    let upto = NaiveDate::parse_from_str("2024-08-01", "%Y-%m-%d").unwrap();
    let maturities = transaction.get_maturities(&upto, account.days_per_year);

    assert_eq!(maturities.len(), 5);

    let expected_days = [
        "2023-08-01",
        "2023-10-31",
        "2024-01-30",
        "2024-04-30",
        "2024-07-30",
    ]
    .iter()
    .map(|date| NaiveDate::parse_from_str(date, "%Y-%m-%d").unwrap())
    .collect::<Vec<_>>();
    let maturity_days = maturities
        .iter()
        .map(|m| m.date())
        .collect::<Vec<NaiveDate>>();

    assert_eq!(expected_days, maturity_days);

    let maturity_return = ANUAL_RETURN / (account.days_per_year as i64);
    let wht = maturity_return * (WHT_RATE as i64) / 100 / 100;
    let expected_return = [maturity_return - wht]
        .into_iter()
        .cycle()
        .take(maturities.len())
        .collect::<Vec<_>>();
    let maturity_return = maturities
        .iter()
        .map(|m| m.amount_paid())
        .collect::<Vec<i64>>();

    assert_eq!(expected_return, maturity_return);
}

#[tokio::test]
async fn maturity_calculation_incorrecte_for_wrong_days_per_year() {
    // Setup

    // 91 days
    const DAYS_TO_MATURE: u16 = 91;
    // [$|Rs.|...] 1,234,567.89
    const INVESTED_AMOUNT: i64 = 123456789;
    // 12.34% per annum
    const YEARLY_INTEREST: u16 = 1234;
    // Number of days per year to consider in retuen calculation
    const ANUAL_RETURN: i64 = INVESTED_AMOUNT
        // Interest is stored as cents (number of cents per 100 rupee)
        * (YEARLY_INTEREST as i64) / 100
        * (DAYS_TO_MATURE as i64)
        // Convert from cents
        / 100;

    let db_pool = get_db_pool().await;
    let portfolio = Portfolio::get_selected(&db_pool).await.unwrap();
    let mut account = fake_account(portfolio.id);

    account.save(&db_pool).await.unwrap();

    let mut transaction = fake_transaction(account.get_id());

    // There should be 4 quarterly maturities per year
    transaction.invested_on = NaiveDate::parse_from_str("2023-08-01", "%Y-%m-%d").unwrap();
    transaction.days_to_maturity = DAYS_TO_MATURE;
    transaction.amount = INVESTED_AMOUNT;
    transaction.rate = YEARLY_INTEREST;

    let upto = NaiveDate::parse_from_str("2024-08-01", "%Y-%m-%d").unwrap();
    let maturities = transaction.get_maturities(&upto, account.days_per_year);

    let expected_return = [ANUAL_RETURN / 999] // <-- WRONG NUMBER OF DAYS PER YEAR
        .into_iter()
        .cycle()
        .take(maturities.len())
        .collect::<Vec<_>>();
    let maturity_return = maturities
        .iter()
        .map(|m| m.amount_paid())
        .collect::<Vec<i64>>();

    assert_ne!(expected_return, maturity_return);
}

#[tokio::test]
async fn maturity_can_only_be_calculated_for_current_transaction() {
    // Setup
    let db_pool = get_db_pool().await;
    let portfolio = Portfolio::get_selected(&db_pool).await.unwrap();
    let mut account = fake_account(portfolio.id);

    account.save(&db_pool).await.unwrap();

    let mut transaction = fake_transaction(account.get_id());
    transaction.days_to_maturity = 30;
    transaction.invested_on = NaiveDate::parse_from_str("2023-04-01", "%Y-%m-%d").unwrap();
    let id = transaction.save(&db_pool).await.unwrap();

    let mut transaction = fake_transaction(account.get_id());
    transaction.days_to_maturity = 30;
    transaction.invested_on = NaiveDate::parse_from_str("2023-08-01", "%Y-%m-%d").unwrap();
    transaction.save(&db_pool).await.unwrap();

    let upto = NaiveDate::parse_from_str("2024-08-01", "%Y-%m-%d").unwrap();
    let transaction_prev = Transaction::get(&db_pool, &account.get_id(), id)
        .await
        .unwrap();
    let maturities_prev = transaction_prev.get_maturities(&upto, account.days_per_year);
    let maturities_current = transaction.get_maturities(&upto, account.days_per_year);

    assert_eq!(maturities_prev.len(), 0);
    assert_eq!(maturities_current.len(), 13);
}
