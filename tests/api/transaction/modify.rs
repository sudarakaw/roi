/* tests/api/transaction/modify.rs
 *
 * Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

use fake::Fake;
use roi::db::{Portfolio, Transaction};

use crate::helper::{fake_account, fake_transaction, get_db_pool};

#[tokio::test]
async fn can_only_modify_current_transaction() {
    // Setup
    let db_pool = get_db_pool().await;
    let portfolio = Portfolio::get_selected(&db_pool).await.unwrap();
    let mut account = fake_account(portfolio.id.clone());

    account.save(&db_pool).await.unwrap();

    let mut transaction1 = fake_transaction(account.get_id());

    transaction1.save(&db_pool).await.unwrap();

    // First transaction should be modifiable.
    let modified_amount = (0..99999999999).fake();
    let modified_rate = (0..9999).fake();
    transaction1.amount = modified_amount;
    transaction1.rate = modified_rate;

    let result = transaction1.save(&db_pool).await;
    assert!(result.is_ok());

    let result = Transaction::get(&db_pool, &account.get_id(), &transaction1.get_id()).await;
    assert!(result.is_ok());
    if let Ok(modifiled_transaction) = result {
        assert_eq!(modifiled_transaction.amount, modified_amount);
        assert_eq!(modifiled_transaction.rate, modified_rate);
    }

    // Create second transaction
    let mut transaction2 = fake_transaction(account.get_id());
    transaction2.invested_on = transaction1.get_maturity_date();

    transaction2.save(&db_pool).await.unwrap();

    // Earlier transaction is no longer modifiable.
    let modified_amount = (0..99999999999).fake();
    let modified_rate = (0..9999).fake();
    transaction1.amount = modified_amount;
    transaction1.rate = modified_rate;

    let result = transaction1.save(&db_pool).await;
    assert!(result.is_err());

    if let Err(msg) = result {
        assert_eq!(
            "Only the current transaction can be modified.",
            msg.to_string()
        );
    }

    // Last (second) transaction is still modifiable.
    let modified_amount = (0..99999999999).fake();
    let modified_rate = (0..9999).fake();
    transaction2.amount = modified_amount;
    transaction2.rate = modified_rate;

    let result = transaction2.save(&db_pool).await;
    assert!(result.is_ok());

    let result = Transaction::get(&db_pool, &account.get_id(), &transaction2.get_id()).await;
    assert!(result.is_ok());
    if let Ok(modifiled_transaction) = result {
        assert_eq!(modifiled_transaction.amount, modified_amount);
        assert_eq!(modifiled_transaction.rate, modified_rate);
    }
}

#[tokio::test]
async fn failing_to_modify_transaction_does_not_remove_it_from_database() {
    // Setup
    let db_pool = get_db_pool().await;
    let portfolio = Portfolio::get_selected(&db_pool).await.unwrap();
    let mut account = fake_account(portfolio.id.clone());

    account.save(&db_pool).await.unwrap();

    let mut transaction = fake_transaction(account.get_id());

    transaction.save(&db_pool).await.unwrap();

    let unmodified_amount = transaction.amount;

    // Modify with bad values
    transaction.amount = 0;

    let result = transaction.save(&db_pool).await;
    assert!(result.is_err());

    let result = Transaction::get(&db_pool, &account.get_id(), &transaction.get_id()).await;
    assert!(result.is_ok());
    if let Ok(modifiled_transaction) = result {
        assert_eq!(modifiled_transaction.get_id(), transaction.get_id());
        assert_eq!(modifiled_transaction.amount, unmodified_amount);
    }
}
