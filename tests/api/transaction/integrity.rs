/* tests/api/transaction/integrity.rs
 *
 * Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

use chrono::{DateTime, Days, Duration, Local, NaiveDate, Utc};
use fake::{faker::chrono::en::DateTimeBefore, Fake};
use roi::db::{Portfolio, Transaction};

use crate::helper::{fake_account, fake_transaction, get_db_pool};

#[tokio::test]
async fn invested_amount_must_be_grater_than_zero() {
    // Setup
    let db_pool = get_db_pool().await;
    let portfolio = Portfolio::get_selected(&db_pool).await.unwrap();
    let invested_on = DateTimeBefore(Local::now().into())
        .fake::<DateTime<Utc>>()
        .naive_local()
        .into();
    let mut account = fake_account(portfolio.id);

    account.save(&db_pool).await.unwrap();

    let mut transaction = Transaction::new(
        account.get_id(),
        (-99999999999..0).fake(),
        (0..9999).fake(),
        (0..9999).fake(),
        invested_on,
        (0..9999).fake(),
    );

    let result = transaction.save(&db_pool).await;

    assert!(result.is_err());

    let error = result.unwrap_err();
    assert_eq!(error.to_string(), "Amount must be larger than zero.");
}

#[tokio::test]
async fn interest_rate_must_be_grater_than_zero() {
    // Setup
    let db_pool = get_db_pool().await;
    let portfolio = Portfolio::get_selected(&db_pool).await.unwrap();
    let invested_on = DateTimeBefore(Local::now().into())
        .fake::<DateTime<Utc>>()
        .naive_local()
        .into();
    let mut account = fake_account(portfolio.id);

    account.save(&db_pool).await.unwrap();

    let mut transaction = Transaction::new(
        account.get_id(),
        (0..99999999999).fake(),
        0,
        (0..9999).fake(),
        invested_on,
        (0..9999).fake(),
    );

    let result = transaction.save(&db_pool).await;

    assert!(result.is_err());

    let error = result.unwrap_err();
    assert_eq!(error.to_string(), "Interest rate must be larger than zero.");
}

#[tokio::test]
async fn maturity_period_must_be_grater_than_zero() {
    // Setup
    let db_pool = get_db_pool().await;
    let portfolio = Portfolio::get_selected(&db_pool).await.unwrap();
    let invested_on = DateTimeBefore(Local::now().into())
        .fake::<DateTime<Utc>>()
        .naive_local()
        .into();

    let mut account = fake_account(portfolio.id);

    account.save(&db_pool).await.unwrap();

    let mut transaction = Transaction::new(
        account.get_id(),
        (0..99999999999).fake(),
        (0..9999).fake(),
        (0..9999).fake(),
        invested_on,
        0,
    );

    let result = transaction.save(&db_pool).await;

    assert!(result.is_err());

    let error = result.unwrap_err();
    assert_eq!(
        error.to_string(),
        "Maturity period must be larger than zero."
    );
}

#[tokio::test]
async fn date_cannot_be_in_future() {
    // Setup
    let db_pool = get_db_pool().await;
    let portfolio = Portfolio::get_selected(&db_pool).await.unwrap();

    // Past transaction
    let mut account = fake_account(portfolio.id.clone());
    account.save(&db_pool).await.unwrap();

    let mut t_past = fake_transaction(account.get_id());
    t_past.invested_on = NaiveDate::parse_from_str("2023-08-01", "%Y-%m-%d").unwrap();

    let result = t_past.save(&db_pool).await;
    assert!(result.is_ok());

    // Today transaction
    let mut account = fake_account(portfolio.id.clone());
    account.save(&db_pool).await.unwrap();

    let mut t_today = fake_transaction(account.get_id());
    // t_today.invested_on = NaiveDate::parse_from_str("2023-08-01", "%Y-%m-%d").unwrap();
    t_today.invested_on = Local::now().naive_local().into();

    let result = t_today.save(&db_pool).await;
    assert!(result.is_ok());

    // Future transaction
    let mut account = fake_account(portfolio.id.clone());
    account.save(&db_pool).await.unwrap();

    let mut t_future = fake_transaction(account.get_id());
    let date = Local::now() + Duration::days(2);
    t_future.invested_on = date.naive_local().into();

    let result = t_future.save(&db_pool).await;
    assert!(result.is_err());
    if let Err(msg) = result {
        assert_eq!(
            "You are not allowed to enter future transactions.",
            msg.to_string()
        );
    }
}

#[tokio::test]
async fn invested_date_cannot_be_before_next_maturity() {
    // Setup
    let db_pool = get_db_pool().await;
    let portfolio = Portfolio::get_selected(&db_pool).await.unwrap();
    let mut account = fake_account(portfolio.id.clone());

    account.save(&db_pool).await.unwrap();

    // Last transaction
    let mut last_transaction = fake_transaction(account.get_id());
    let last_maturity = last_transaction.get_maturity_date();

    last_transaction.save(&db_pool).await.unwrap();

    // Next Transaction
    let mut next_transaction = fake_transaction(account.get_id());
    next_transaction.invested_on = last_maturity - Days::new(1);

    let result = next_transaction.save(&db_pool).await;
    assert!(result.is_err());
    if let Err(msg) = result {
        assert_eq!(
            format!(
                "Investment cannot mature before {}",
                last_maturity.format("%Y-%m-%d")
            ),
            msg.to_string()
        );
    }
}

#[tokio::test]
async fn next_maturity_calculated_using_last_transaction_when_modifying_current() {
    // Setup
    let db_pool = get_db_pool().await;
    let portfolio = Portfolio::get_selected(&db_pool).await.unwrap();
    let mut account = fake_account(portfolio.id.clone());

    account.save(&db_pool).await.unwrap();

    let period = 30;

    let mut first_transaction = fake_transaction(account.get_id());
    first_transaction.days_to_maturity = period;
    first_transaction.invested_on = (Local::now() - Duration::days(period as i64 * 4))
        .naive_local()
        .into();
    let first_maturity = first_transaction.get_maturity_date();

    let mut second_transaction = fake_transaction(account.get_id());
    second_transaction.days_to_maturity = period;
    second_transaction.invested_on = first_maturity;
    let second_maturity = second_transaction.get_maturity_date();

    let mut current_transaction = fake_transaction(account.get_id());
    current_transaction.days_to_maturity = period;
    current_transaction.invested_on = second_maturity;

    first_transaction.save(&db_pool).await.unwrap();

    // Modify only transaction of the account
    first_transaction.amount = 999999;
    let result = first_transaction.save(&db_pool).await;
    assert!(result.is_ok());

    second_transaction.save(&db_pool).await.unwrap();
    current_transaction.save(&db_pool).await.unwrap();

    // Modify current Transaction
    current_transaction.amount = 10000;
    current_transaction.invested_on = second_maturity + Days::new(5);

    let result = current_transaction.save(&db_pool).await;
    assert!(result.is_ok());

    // Modify current Transaction with bad date
    current_transaction.amount = 20000;
    current_transaction.invested_on = second_maturity - Days::new(5);

    let result = current_transaction.save(&db_pool).await;
    assert!(result.is_err());
    if let Err(msg) = result {
        assert_eq!(
            format!(
                "Investment cannot mature before {}",
                second_maturity.format("%Y-%m-%d")
            ),
            msg.to_string()
        );
    }
}
