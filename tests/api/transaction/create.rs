/* tests/api/transaction/create.rs
 *
 * Copyright 2022 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

use roi::db::{Portfolio, Transaction};

use crate::helper::{fake_account, fake_transaction, get_db_pool};

#[tokio::test]
async fn cannot_be_saved_with_invalid_account() {
    // Setup
    let db_pool = get_db_pool().await;
    let portfolio = Portfolio::get_selected(&db_pool).await.unwrap();
    let invalid_account = fake_account(portfolio.id.clone());
    let mut valid_account = fake_account(portfolio.id.clone());
    let mut archived_account = fake_account(portfolio.id);

    valid_account.save(&db_pool).await.unwrap();
    archived_account.archive(&db_pool).await.unwrap();

    // Transaction using invalid account
    let mut invalid_account_transaction = fake_transaction(invalid_account.get_id());
    let invalid_account_transaction_result = invalid_account_transaction.save(&db_pool).await;

    assert!(invalid_account_transaction_result.is_err());

    if let Err(msg) = invalid_account_transaction_result {
        assert_eq!(
            "Attempt to create transaction using an invalid account.",
            msg.to_string()
        );
    }

    // Transaction using archived account
    let mut archived_account_transaction = fake_transaction(archived_account.get_id());
    let archived_account_transaction_result = archived_account_transaction.save(&db_pool).await;

    assert!(archived_account_transaction_result.is_err());

    if let Err(msg) = archived_account_transaction_result {
        assert_eq!(
            "Attempt to create transaction using an archived account.",
            msg.to_string()
        );
    }

    let mut valid_account_transaction = fake_transaction(valid_account.get_id());

    let result = valid_account_transaction.save(&db_pool).await;
    assert!(result.is_ok());
}

#[tokio::test]
async fn new_transaction_keep_current_status() {
    // Setup
    let db_pool = get_db_pool().await;
    let portfolio = Portfolio::get_selected(&db_pool).await.unwrap();
    let mut account = fake_account(portfolio.id);

    account.save(&db_pool).await.unwrap();

    // Create first transaction
    let mut first_transaction = fake_transaction(account.get_id());
    let first_maturity = first_transaction.get_maturity_date();

    let first_transaction_id = first_transaction.save(&db_pool).await.unwrap();
    let current_transaction_id = Transaction::get_current(&db_pool, &account.get_id())
        .await
        .unwrap()
        .get_id();

    assert_eq!(*first_transaction_id, current_transaction_id);

    // Create second transaction
    let mut second_transaction = fake_transaction(account.get_id());
    second_transaction.invested_on = first_maturity;

    let second_transaction_id = second_transaction.save(&db_pool).await.unwrap();
    let current_transaction_id = Transaction::get_current(&db_pool, &account.get_id())
        .await
        .unwrap()
        .get_id();

    assert_eq!(*second_transaction_id, current_transaction_id);

    assert_ne!(*first_transaction_id, current_transaction_id);
}

#[tokio::test]
async fn current_status_only_affect_own_account() {
    // Setup
    let db_pool = get_db_pool().await;
    let portfolio = Portfolio::get_selected(&db_pool).await.unwrap();
    let mut account1 = fake_account(portfolio.id.clone());
    let mut account2 = fake_account(portfolio.id);

    account1.save(&db_pool).await.unwrap();
    account2.save(&db_pool).await.unwrap();

    // Create transactions on first account
    let mut a1t1 = fake_transaction(account1.get_id());
    let a1t1_maturity = a1t1.get_maturity_date();
    a1t1.save(&db_pool).await.unwrap();
    let mut a1t2 = fake_transaction(account1.get_id());
    a1t2.invested_on = a1t1_maturity;
    a1t2.save(&db_pool).await.unwrap();

    // Create transactions on second account
    let mut a2t1 = fake_transaction(account2.get_id());
    let a2t1_maturity = a2t1.get_maturity_date();
    a2t1.save(&db_pool).await.unwrap();
    let mut a2t2 = fake_transaction(account2.get_id());
    a2t2.invested_on = a2t1_maturity;
    a2t2.save(&db_pool).await.unwrap();

    let account1_transaction_id = Transaction::get_current(&db_pool, &account1.get_id())
        .await
        .unwrap()
        .get_id();

    let account2_transaction_id = Transaction::get_current(&db_pool, &account2.get_id())
        .await
        .unwrap()
        .get_id();

    assert_ne!(account1_transaction_id, account2_transaction_id);
}
