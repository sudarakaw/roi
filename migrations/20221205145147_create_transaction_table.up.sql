-- migrations/20221205145147_create_transaction_table.up.sql
--
-- Copyright 2022 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
--
-- This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
-- you are welcome to redistribute it and/or modify it under the terms of the
-- BSD 2-clause License. See the LICENSE file for more details.
--
CREATE    TABLE if NOT EXISTS `transaction` (
          `id` BLOB PRIMARY KEY NOT NULL
        , `account` BLOB NOT NULL
        , `amount` INT NOT NULL CHECK (0 <= `amount`)
        , `rate` INT NOT NULL CHECK (
          0 < `rate`
AND       10000 > `rate`
          )
        , `invested_on` VARCHAR(32) NOT NULL
        , `days_to_maturity` INT NOT NULL CHECK (0 < `days_to_maturity`)
        , `current` tinyint NOT NULL CHECK (
          0 = `current`
OR        1 = `current`
          )
        , FOREIGN key (`account`) REFERENCES `account` (`id`) ON DELETE CASCADE
          );
