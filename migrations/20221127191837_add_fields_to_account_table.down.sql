-- migrations/20221127191837_add_fields_to_account_table.down.sql
--
-- Copyright 2022 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
--
-- This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
-- you are welcome to redistribute it and/or modify it under the terms of the
-- BSD 2-clause License. See the LICENSE file for more details.
--
ALTER     TABLE `account`
DROP      COLUMN `type`;


ALTER     TABLE `account`
DROP      COLUMN `number`;


ALTER     TABLE `account`
DROP      COLUMN `archived`;
