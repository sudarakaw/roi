-- migrations/20241204062743_add_wht_rate_field_to_transaction_table.up.sql
--
-- Copyright 2024 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
--
-- This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
-- you are welcome to redistribute it and/or modify it under the terms of the
-- BSD 2-clause License. See the LICENSE file for more details.
--

ALTER     TABLE `transaction`
ADD       COLUMN `wht_rate` INT NOT NULL  DEFAULT 0 CHECK (
          0 <= `wht_rate`
AND       10000 > `wht_rate`
          );

