-- migrations/20241203112034_add_days_per_year_field_to_accounts_table.down.sql
--
-- Copyright 2024 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
--
-- This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
-- you are welcome to redistribute it and/or modify it under the terms of the
-- BSD 2-clause License. See the LICENSE file for more details.
--
ALTER     TABLE `account`
DROP      COLUMN `days_per_year`;
