-- migrations/20220909073048_create_portfolio_table.up.sql
--
-- Copyright 2022 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
--
-- This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
-- you are welcome to redistribute it and/or modify it under the terms of the
-- BSD 2-clause License. See the LICENSE file for more details.
--
CREATE    TABLE IF NOT EXISTS `portfolio` (
          `id` BLOB PRIMARY KEY NOT NULL
        , `name` VARCHAR(32) NOT NULL UNIQUE
        , `selected` TINYINT NOT NULL CHECK (
          0 = `selected`
OR        1 = `selected`
          )
          );
