-- migrations/20221127191837_add_fields_to_account_table.up.sql
--
-- Copyright 2022 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
--
-- This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
-- you are welcome to redistribute it and/or modify it under the terms of the
-- BSD 2-clause License. See the LICENSE file for more details.
--
ALTER     TABLE `account`
ADD       COLUMN `type` VARCHAR(8) NOT NULL CHECK (
'FD' = `type`
OR        'TBILL' = `type`
OR        'SAVINGS' = `type`
OR        'MONEYMKT' = `type`
          );


ALTER     TABLE `account`
ADD       COLUMN `number` VARCHAR(32) NOT NULL;


ALTER     TABLE `account`
ADD       COLUMN `archived` tinyint NOT NULL CHECK (
0 = `archived`
OR        1 = `archived`
          );
