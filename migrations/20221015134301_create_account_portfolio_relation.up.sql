-- migrations/20221015134301_create_account_table.up.sql
--
-- Copyright 2022 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
--
-- This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
-- you are welcome to redistribute it and/or modify it under the terms of the
-- BSD 2-clause License. See the LICENSE file for more details.
--
CREATE    TABLE if NOT EXISTS `account` (
          `id` BLOB PRIMARY KEY NOT NULL
        , `portfolio` BLOB NOT NULL
        , FOREIGN key (`portfolio`) REFERENCES `portfolio` (`id`) ON DELETE CASCADE
          );
