# roi (Return On Investment Calculator)

Calculate/Show return on investment from list of investment accounts on record.

## Development Setup

```sh
git clone https://gitlab.com/sudarakaw/roi

cd ./roi

git submodule update --init --remote

./build-helper/bootstrap.sh

./configure --enable-debug

make init

```

### To run development version of the application

```sh
make start

```

### To run continuous tests for application/data module

```sh
./configure --enable-debug --with-watch-command="'test --manifest-path ./Cargo.toml --target=x86_64-unknown-linux-gnu'"

make watch-rust

```

### To run continuous tests for UI module

```sh
./configure --enable-debug --with-watch-command="'test --manifest-path ./ui/Cargo.toml --target=x86_64-unknown-linux-gnu'"

make watch-rust

```


## License

Copyright 2016 Sudaraka Wijesinghe <sudaraka@sudaraka.org>

This program comes with ABSOLUTELY NO WARRANTY;
This is free software, and you are welcome to redistribute it and/or modify it
under the terms of the BSD 2-clause License. See the LICENSE file for more
details.
