/* src/config.rs
 *
 * Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

//! Load configuration from filesystem and/or environment

use std::{fs::create_dir_all, path::PathBuf};

use anyhow::{Context, Result};
use directories::ProjectDirs;
use dotenvy::dotenv;
use serde::Deserialize;

use crate::package;

#[derive(Debug, Deserialize)]
#[serde(default)]
pub struct Config {
    pub db_path: PathBuf,
}

impl Config {
    pub fn new() -> Result<Self> {
        dotenv().ok();

        let dirs = match ProjectDirs::from("dev", "suda", &Self::app_name()) {
            None => {
                return Ok(Self::default());
            }
            Some(d) => d,
        };
        let config_file = dirs.config_local_dir().join("config");

        config::Config::builder()
            .add_source(
                config::File::with_name(config_file.to_str().unwrap())
                    .required(false)
                    .format(config::FileFormat::Ini),
            )
            .add_source(config::Environment::with_prefix(package::NAME))
            .build()
            .with_context(|| "Reading configuration files & environment")?
            .try_deserialize::<Self>()
            .with_context(|| "Decoding configuration data")
    }

    fn app_name() -> String {
        #[cfg(target_os = "linux")]
        return format!("dev.suda.{}", package::NAME);
        #[cfg(not(target_os = "linux"))]
        return package::NAME.as_string();
    }
}

impl Default for Config {
    fn default() -> Self {
        let path = match ProjectDirs::from("dev", "suda", &Self::app_name()) {
            None => PathBuf::from("/var/lib").join(package::NAME),
            Some(d) => d.data_local_dir().to_owned(),
        };

        // Attempt to create database path if it doesn't exists.
        create_dir_all(&path).unwrap_or(());

        Self {
            db_path: path.join("database"),
        }
    }
}
