/* src/lib.rs
 *
 * Copyright 2022 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

//! ROI main module

#[cfg(feature = "app")]
use sqlx::SqlitePool;

#[cfg(feature = "app")]
mod config;

#[cfg(feature = "app")]
pub use crate::config::Config;

#[cfg(feature = "app")]
pub mod command;

pub mod db;
pub mod package;

#[cfg(feature = "app")]
pub struct DB(pub SqlitePool);
