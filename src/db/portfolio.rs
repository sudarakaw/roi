/* src/db/portfolio.rs
 *
 * Copyright 2022 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

//! This module represent an investment portfolio which contains a collection
//! of accounts.

#[cfg(feature = "app")]
use anyhow::{anyhow, Result};
use serde::{Deserialize, Serialize};
#[cfg(feature = "app")]
use sqlx::SqlitePool;
use uuid::Uuid;

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize)]
#[cfg_attr(feature = "app", derive(sqlx::Type), sqlx(transparent))]
pub struct PortfolioId(Uuid);

impl PortfolioId {
    /// Create new `PortfolioId` instance with random UUID.
    fn new() -> Self {
        Self(Uuid::new_v4())
    }

    /// Verify existance of this `PortfolioId` in the databse.
    #[cfg(feature = "app")]
    pub async fn exists(&self, db_pool: &SqlitePool) -> Result<()> {
        sqlx::query!(
            r#"
            SELECT `id`
            FROM `portfolio`
            WHERE `id` = $1
            "#,
            self
        )
        .fetch_one(db_pool)
        .await?;

        Ok(())
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct Portfolio {
    pub id: PortfolioId,
    pub name: String,
    pub selected: bool,
}

impl Portfolio {
    /// Create new instance of `Portfolio` with given values & a new `id`.
    pub fn new(name: String, selected: bool) -> Self {
        let id = PortfolioId::new();

        Self { id, name, selected }
    }

    #[cfg(feature = "app")]
    pub async fn get(db_pool: &SqlitePool, id: &PortfolioId) -> Result<Self> {
        let row = sqlx::query_as!(
            Self,
            r#"
            SELECT `id` as "id!: PortfolioId"
                 , `name`
                 , `selected` as "selected!: bool"
            FROM `portfolio`
            WHERE `id` = $1
            "#,
            id
        )
        .fetch_one(db_pool)
        .await?;

        Ok(row)
    }

    /// Get all records from database and return the `Portfolio` instances of them.
    #[cfg(feature = "app")]
    pub async fn get_all(db_pool: &SqlitePool) -> Vec<Self> {
        let result = sqlx::query_as!(
            Self,
            r#"
            SELECT `id` as "id!: PortfolioId"
                 , `name`
                 , `selected` as "selected!: bool"
            FROM `portfolio`
            "#
        )
        .fetch_all(db_pool)
        .await;

        match result {
            Ok(rows) => rows.to_vec(),
            _ => vec![],
        }
    }

    /// Get the `selected` record from database and return the `Portfolio` instance of it.
    #[cfg(feature = "app")]
    pub async fn get_selected(db_pool: &SqlitePool) -> Result<Self> {
        let rows = sqlx::query_as!(
            Self,
            r#"
            SELECT `id` as "id!: PortfolioId"
                 , `name`
                 , `selected` as "selected!: bool"
            FROM `portfolio`
            WHERE `selected` = 1
            "#
        )
        .fetch_all(db_pool)
        .await?;

        match &rows[..] {
            [selected] => Ok(selected.clone()),
            [] => Err(anyhow::anyhow!("No selected portfolio records found.")),
            _ => Err(anyhow::anyhow!(
                "More than one selected portfolio records found."
            )),
        }
    }

    /// Store state of the current instance of `Portfolio` in database.
    #[cfg(feature = "app")]
    pub async fn save(&mut self, db_pool: &SqlitePool) -> Result<&PortfolioId> {
        if self.name.trim().is_empty() {
            anyhow::bail!("Portfolio name must be non-empty & unique.");
        }

        match Self::get(db_pool, &self.id).await {
            Ok(_) => self.update(db_pool).await,
            Err(_) => self.create(db_pool).await,
        }
    }

    /// Remove current instance of `Portfolio` from the database.
    #[cfg(feature = "app")]
    pub async fn remove(&self, db_pool: &SqlitePool) -> Result<()> {
        let selected_portfolio = Self::get_selected(db_pool).await?;

        if selected_portfolio.id == self.id || self.selected {
            // Current portfolio is selected

            anyhow::bail!("Currently selected portfolio cannot be removed. Set another portfolio as selected before removing this one.");
        }

        sqlx::query!(
            r#"
            DELETE FROM `portfolio`
            WHERE `id` = $1
            "#,
            self.id
        )
        .execute(db_pool)
        .await?;

        Ok(())
    }

    #[cfg(feature = "app")]
    async fn create(&mut self, db_pool: &SqlitePool) -> Result<&PortfolioId> {
        if Self::get_selected(db_pool).await.is_err() {
            // When there are no selected portfolios (i.e `get_selected` return
            // an error), the one being creared must be set as selected.

            self.selected = true;
        } else if self.selected {
            // Selected portfolio already exists & we are about to create another selected
            // portfolio. Clear the selected flag of existing portfolio(s) in database.

            sqlx::query!(
                r#"
                UPDATE `portfolio`
                SET `selected` = 0
                WHERE `selected` = 1
                "#
            )
            .execute(db_pool)
            .await?;
        }

        sqlx::query!(
            r#"
            INSERT INTO `portfolio` (`id`, `name`, `selected`)
            VALUES ($1, $2, $3)
            "#,
            self.id,
            self.name,
            self.selected
        )
        .execute(db_pool)
        .await
        .map_err(|e| {
            if let sqlx::Error::Database(err) = &e {
                if sqlx::error::ErrorKind::UniqueViolation == err.kind()
                    && err.message().ends_with(".name")
                {
                    return anyhow!("Portfolio with name \"{}\" already exists.", self.name);
                }
            }

            e.into()
        })?;

        Ok(&self.id)
    }

    #[cfg(feature = "app")]
    async fn update(&self, db_pool: &SqlitePool) -> Result<&PortfolioId> {
        if self.selected {
            // Setting portfolio as selected,
            // Set currently selected account(s) as unselected.

            sqlx::query!(
                r#"
                UPDATE `portfolio`
                SET `selected` = 0
                WHERE `selected` = 1
                "#
            )
            .execute(db_pool)
            .await?;
        } else {
            // Setting portfolio as unselected,

            let selected_portfolio = Self::get_selected(db_pool).await?;

            if selected_portfolio.id == self.id {
                // Current portfolio is selected

                anyhow::bail!("Currently selected portfolio cannot be unselected. Create/Set another portfolio as selected instead.");
            }
        }

        sqlx::query!(
            r#"
            UPDATE `portfolio`
            SET `name` = $2
              , `selected` = $3
            WHERE `id` = $1
            "#,
            self.id,
            self.name,
            self.selected
        )
        .execute(db_pool)
        .await
        .map_err(|e| {
            if let sqlx::Error::Database(err) = &e {
                if sqlx::error::ErrorKind::UniqueViolation == err.kind()
                    && err.message().ends_with(".name")
                {
                    return anyhow!("Portfolio with name \"{}\" already exists.", self.name);
                }
            }

            e.into()
        })?;

        Ok(&self.id)
    }
}

impl From<PortfolioId> for String {
    fn from(value: PortfolioId) -> Self {
        value.0.to_string()
    }
}
