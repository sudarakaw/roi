/* src/db/account.rs
 *
 * Copyright 2022 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

//! This module represent an single investment account.

use std::{fmt, str::FromStr};

#[cfg(feature = "app")]
use anyhow::{bail, Result};
use serde::{Deserialize, Serialize};
#[cfg(feature = "app")]
use sqlx::SqlitePool;
use uuid::Uuid;

use super::PortfolioId;

#[cfg_attr(feature = "app", derive(sqlx::Type), sqlx(transparent))]
#[derive(Clone, Copy, Debug, Deserialize, Serialize, PartialEq, Eq, Hash)]
pub struct AccountId(Uuid);

impl AccountId {
    /// Create new `AccountId` instance with random UUID.
    fn new() -> Self {
        Self(Uuid::new_v4())
    }

    /// Verify existance of this `AccountId` in the databse.
    #[cfg(feature = "app")]
    pub async fn exists(&self, db_pool: &SqlitePool) -> Result<bool> {
        let result = sqlx::query!(
            r#"
            SELECT `archived` as "archived!: bool"
            FROM `account`
            WHERE `id` = $1
            "#,
            self
        )
        .fetch_one(db_pool)
        .await?;

        Ok(result.archived)
    }
}

impl FromStr for AccountId {
    type Err = uuid::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let uuid = Uuid::from_str(s)?;

        Ok(Self(uuid))
    }
}

impl fmt::Display for AccountId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

#[cfg_attr(feature = "app", derive(sqlx::Type))]
#[derive(Debug, Default, Clone, Deserialize, Serialize, PartialEq, Eq, PartialOrd, Ord)]
pub enum AccountType {
    #[cfg_attr(feature = "app", sqlx(rename = "TBILL"))]
    TresuryBill,
    #[cfg_attr(feature = "app", sqlx(rename = "FD"))]
    FixedDeposit,
    #[cfg_attr(feature = "app", sqlx(rename = "SAVINGS"))]
    #[default]
    Savings,
    #[cfg_attr(feature = "app", sqlx(rename = "MONEYMKT"))]
    MoneyMarket,
}

impl AccountType {
    pub fn list() -> Vec<Self> {
        vec![
            Self::Savings,
            Self::FixedDeposit,
            Self::TresuryBill,
            Self::MoneyMarket,
        ]
    }
}

impl fmt::Display for AccountType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Savings => write!(f, "Savings"),
            Self::FixedDeposit => write!(f, "Fixed Deposit"),
            Self::TresuryBill => write!(f, "Tresury Bill"),
            Self::MoneyMarket => write!(f, "Money Market"),
        }
    }
}

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
pub struct Account {
    id: AccountId,
    portfolio_id: PortfolioId,
    pub account_type: AccountType,
    pub number: String,
    pub archived: bool,
    pub days_per_year: u16,
}

impl Account {
    /// Create new `Account` instance with given values.
    pub fn new(
        portfolio_id: PortfolioId,
        account_type: AccountType,
        number: String,
        archived: bool,
        days_per_year: u16,
    ) -> Self {
        let id = AccountId::new();

        Account {
            id,
            portfolio_id,
            account_type,
            number,
            archived,
            days_per_year,
        }
    }

    /// Return the `AccountId` of `Account` instance to the caller.
    pub fn get_id(&self) -> AccountId {
        self.id
    }

    /// Store state of the current instance of `Account` in database.
    #[cfg(feature = "app")]
    pub async fn save(&mut self, db_pool: &SqlitePool) -> Result<&AccountId> {
        if self.portfolio_id.exists(db_pool).await.is_err() {
            bail!("Attempt to create account for invalid portfolio.");
        }

        if self.number.trim().is_empty() {
            anyhow::bail!("Account number must not be empty.");
        }

        match Self::get(db_pool, &self.id).await {
            Ok(_) => self.update(db_pool).await,
            Err(_) => self.create(db_pool).await,
        }
    }

    /// Set archive state of current `Account` instance & update database.
    #[cfg(feature = "app")]
    pub async fn archive(&mut self, db_pool: &SqlitePool) -> Result<()> {
        self.archived = true;

        self.save(db_pool).await?;

        Ok(())
    }

    #[cfg(feature = "app")]
    async fn get(db_pool: &SqlitePool, id: &AccountId) -> Result<Account> {
        let row = sqlx::query_as!(
            Self,
            r#"
            SELECT `id` as "id!: AccountId"
                 , `portfolio` as "portfolio_id!: PortfolioId"
                 , `type` as "account_type!: AccountType"
                 , `number`
                 , `archived` as "archived!: bool"
                 , `days_per_year` as "days_per_year!: u16"
            FROM `account`
            WHERE `id` = $1
            "#,
            id
        )
        .fetch_one(db_pool)
        .await?;

        Ok(row)
    }

    #[cfg(feature = "app")]
    async fn create(&self, db_pool: &SqlitePool) -> Result<&AccountId> {
        sqlx::query!(
            r#"
            INSERT INTO `account`
                ( `id`
                , `portfolio`
                , `type`
                , `number`
                , `archived`
                , `days_per_year`
                )
            VALUES ($1, $2, $3, $4, $5, $6)
            "#,
            self.id,
            self.portfolio_id,
            self.account_type,
            self.number,
            self.archived,
            self.days_per_year,
        )
        .execute(db_pool)
        .await?;

        Ok(&self.id)
    }

    #[cfg(feature = "app")]
    async fn update(&self, db_pool: &SqlitePool) -> Result<&AccountId> {
        sqlx::query!(
            r#"
            UPDATE `account`
            SET `id` = $1
              , `type` = $2
              , `number` = $3
              , `archived` = $4
              , `days_per_year` = $5
            WHERE `id` = $1
            "#,
            self.id,
            self.account_type,
            self.number,
            self.archived,
            self.days_per_year,
        )
        .execute(db_pool)
        .await?;

        Ok(&self.id)
    }

    /// Get all records from database and return the `Account` instances of them.
    #[cfg(feature = "app")]
    pub async fn get_all(db_pool: &SqlitePool, portfolio_id: &PortfolioId) -> Vec<Account> {
        let result = sqlx::query_as!(
            Self,
            r#"
            SELECT `id` as "id!: AccountId"
                 , `portfolio` as "portfolio_id!: PortfolioId"
                 , `type` as "account_type!: AccountType"
                 , `number`
                 , `archived` as "archived!: bool"
                 , `days_per_year` as "days_per_year!: u16"
            FROM `account`
            WHERE `portfolio` = $1
            "#,
            portfolio_id
        )
        .fetch_all(db_pool)
        .await;

        match result {
            Ok(rows) => rows.to_vec(),
            _ => vec![],
        }
    }
}
