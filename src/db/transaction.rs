/* src/db/transaction.rs
 *
 * Copyright 2022 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

//! This module represent an single tractions in a history of an account.

#[cfg(feature = "app")]
use anyhow::{bail, Result};
use chrono::Duration;
#[cfg(feature = "app")]
use chrono::Local;
#[cfg(not(feature = "app"))]
use chrono::NaiveDate;
use serde::{Deserialize, Serialize};
#[cfg(feature = "app")]
use sqlx::types::chrono::NaiveDate;
#[cfg(feature = "app")]
use sqlx::SqlitePool;
use uuid::Uuid;

use super::AccountId;

#[cfg_attr(feature = "app", derive(sqlx::Type), sqlx(transparent))]
#[derive(Debug, PartialEq, Clone, Copy, Serialize, Deserialize)]
pub struct TransactionId(Uuid);

impl TransactionId {
    /// Create new `TransactionId` instance with random UUID.
    fn new() -> TransactionId {
        TransactionId(Uuid::new_v4())
    }
}

#[cfg_attr(feature = "app", derive(sqlx::Type))]
#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
pub struct Transaction {
    id: TransactionId,
    account_id: AccountId,
    pub amount: i64,
    pub rate: u16,
    pub wht_rate: u16,
    pub invested_on: NaiveDate,
    pub days_to_maturity: u16,
    current: bool,
}

#[cfg(feature = "app")]
impl Transaction {
    async fn validate(&self) -> Result<()> {
        // Static validations
        if 0 >= self.amount {
            anyhow::bail!("Amount must be larger than zero.");
        }

        if 0 == self.rate {
            anyhow::bail!("Interest rate must be larger than zero.");
        }

        let now = Local::now().naive_local().into();
        if self.invested_on.cmp(&now).is_gt() {
            bail!("You are not allowed to enter future transactions.");
        }

        if 0 == self.days_to_maturity {
            anyhow::bail!("Maturity period must be larger than zero.");
        }

        Ok(())
    }

    async fn get_one_before(&self, db_pool: &SqlitePool) -> Result<Transaction> {
        let row = sqlx::query_as!(
            Self,
            r#"
            SELECT `id` as "id!: TransactionId"
                 , `account` as "account_id!: AccountId"
                 , `amount` as "amount!: i64"
                 , `rate` as "rate!: u16"
                 , `wht_rate` as "wht_rate!: u16"
                 , `invested_on` as "invested_on!: NaiveDate"
                 , `days_to_maturity` as "days_to_maturity!: u16"
                 , `current` as "current!: bool"
            FROM `transaction`
            WHERE `account` = $1
              AND `invested_on` < $2
            ORDER BY `invested_on` DESC
            LIMIT 1
            "#,
            self.account_id,
            self.invested_on
        )
        .fetch_one(db_pool)
        .await?;

        Ok(row)
    }

    pub async fn get_current(db_pool: &SqlitePool, account_id: &AccountId) -> Result<Transaction> {
        let rows = sqlx::query_as!(
            Self,
            r#"
            SELECT `id` as "id!: TransactionId"
                 , `account` as "account_id!: AccountId"
                 , `amount` as "amount!: i64"
                 , `rate` as "rate!: u16"
                 , `wht_rate` as "wht_rate!: u16"
                 , `invested_on` as "invested_on!: NaiveDate"
                 , `days_to_maturity` as "days_to_maturity!: u16"
                 , `current` as "current!: bool"
            FROM `transaction`
            WHERE `account` = $1
              AND `current` = 1
            "#,
            account_id
        )
        .fetch_all(db_pool)
        .await?;

        match &rows[..] {
            [current] => Ok(current.clone()),
            [] => Err(anyhow::anyhow!("No current transaction records found.")),
            _ => Err(anyhow::anyhow!(
                "More than one transaction records are marked as current found."
            )),
        }
    }

    /// Store state of the current instance of `Transaction` in database.
    pub async fn save(&mut self, db_pool: &SqlitePool) -> Result<&TransactionId> {
        let account_validation = self.account_id.exists(db_pool).await;

        if account_validation.is_err() {
            bail!("Attempt to create transaction using an invalid account.");
        } else if account_validation.unwrap() {
            bail!("Attempt to create transaction using an archived account.");
        }

        self.validate().await?;

        let this_transaction = Self::get(db_pool, &self.account_id, &self.id).await;

        if let Ok(this_transaction) = this_transaction {
            if !this_transaction.current {
                bail!("Only the current transaction can be modified.");
            }

            // Validate invested date against next maturity date of last
            // transaction of the account if one exists
            if let Ok(last_transaction) = this_transaction.get_one_before(db_pool).await {
                self.bail_if_invested_before(last_transaction.get_maturity_date())?;
            }

            self.update(db_pool).await
        } else {
            // Validate invested date against next maturity date of current
            // transaction of the account if one exists
            if let Ok(current_transaction) =
                Transaction::get_current(db_pool, &self.account_id).await
            {
                self.bail_if_invested_before(current_transaction.get_maturity_date())?;
            }

            self.create(db_pool).await
        }
    }

    fn bail_if_invested_before(&self, date: NaiveDate) -> Result<()> {
        if date > self.invested_on {
            anyhow::bail!(format!(
                "Investment cannot mature before {}",
                date.format("%Y-%m-%d")
            ));
        }

        Ok(())
    }

    async fn update(&self, db_pool: &SqlitePool) -> Result<&TransactionId> {
        sqlx::query!(
            r#"
            UPDATE `transaction`
               SET `amount` = $3
                 , `rate` = $4
                 , `invested_on` = $5
                 , `days_to_maturity` = $6
                 , `wht_rate` = $7
             WHERE `id` = $1
               AND `account` = $2
            "#,
            self.id,
            self.account_id,
            self.amount,
            self.rate,
            self.invested_on,
            self.days_to_maturity,
            self.wht_rate,
        )
        .execute(db_pool)
        .await?;

        Ok(&self.id)
    }

    pub async fn get(
        db_pool: &SqlitePool,
        account_id: &AccountId,
        id: &TransactionId,
    ) -> Result<Transaction> {
        let row = sqlx::query_as!(
            Self,
            r#"
            SELECT `id` as "id!: TransactionId"
                 , `account` as "account_id!: AccountId"
                 , `amount` as "amount!: i64"
                 , `rate` as "rate!: u16"
                 , `wht_rate` as "wht_rate!: u16"
                 , `invested_on` as "invested_on!: NaiveDate"
                 , `days_to_maturity` as "days_to_maturity!: u16"
                 , `current` as "current!: bool"
            FROM `transaction`
            WHERE `id` = $1
              AND `account` = $2
            "#,
            id,
            account_id
        )
        .fetch_one(db_pool)
        .await?;

        Ok(row)
    }

    pub async fn get_all(db_pool: &SqlitePool, account_id: &AccountId) -> Vec<Transaction> {
        let result = sqlx::query_as!(
            Self,
            r#"
            SELECT `id` as "id!: TransactionId"
                 , `account` as "account_id!: AccountId"
                 , `amount` as "amount!: i64"
                 , `rate` as "rate!: u16"
                 , `wht_rate` as "wht_rate!: u16"
                 , `invested_on` as "invested_on!: NaiveDate"
                 , `days_to_maturity` as "days_to_maturity!: u16"
                 , `current` as "current!: bool"
            FROM `transaction`
            WHERE `account` = $1
            ORDER BY `invested_on` DESC
            "#,
            account_id
        )
        .fetch_all(db_pool)
        .await;

        match result {
            Ok(rows) => rows.to_vec(),
            _ => vec![],
        }
    }

    async fn create(&self, db_pool: &SqlitePool) -> Result<&TransactionId> {
        sqlx::query!(
            r#"
                UPDATE `transaction`
                SET `current` = 0
                WHERE `account` = $1
                  AND `current` = 1
            "#,
            self.account_id
        )
        .execute(db_pool)
        .await?;

        sqlx::query!(
            r#"
            INSERT INTO `transaction`
                ( `id`
                , `account`
                , `amount`
                , `rate`
                , `invested_on`
                , `days_to_maturity`
                , `current`
                , `wht_rate`
                )
            VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
            "#,
            self.id,
            self.account_id,
            self.amount,
            self.rate,
            self.invested_on,
            self.days_to_maturity,
            self.current,
            self.wht_rate,
        )
        .execute(db_pool)
        .await?;

        Ok(&self.id)
    }
}

impl Transaction {
    pub fn new(
        account_id: AccountId,
        amount: i64,
        rate: u16,
        wht_rate: u16,
        invested_on: NaiveDate,
        days_to_maturity: u16,
    ) -> Self {
        let id = TransactionId::new();

        Transaction {
            id,
            account_id,
            amount,
            rate,
            wht_rate,
            invested_on,
            days_to_maturity,
            current: true,
        }
    }

    /// Return the `TransactionId` of `Transaction` instance to the caller.
    pub fn get_id(&self) -> TransactionId {
        self.id
    }

    /// Return the associated `AccountId` of `Transaction` instance to the caller.
    pub fn get_account_id(&self) -> AccountId {
        self.account_id
    }

    /// Return maturity date of current instance
    pub fn get_maturity_date(&self) -> NaiveDate {
        self.invested_on + Duration::days(self.days_to_maturity.into())
    }

    pub fn get_maturities(&self, upto: &NaiveDate, days_per_year: u16) -> Vec<Maturity> {
        let mut maturities = vec![];

        if !self.current {
            // Only maturities of current transaction os the account can be calculated.
            return maturities;
        }

        let mut next = self.get_maturity_date();
        let last = next - Duration::days(self.days_to_maturity.into());
        let maturity_return = self.amount * (self.rate as i64) * (self.days_to_maturity as i64)
            / (days_per_year as i64)
            / 100
            / 100;
        let wht = maturity_return * (self.wht_rate as i64) / 100 / 100;

        maturities.push(Maturity::new(last, maturity_return, wht));

        while &next <= upto {
            let maturity_return = self.amount * (self.rate as i64) * (self.days_to_maturity as i64)
                / (days_per_year as i64)
                / 100
                / 100;
            let wht = maturity_return * (self.wht_rate as i64) / 100 / 100;

            maturities.push(Maturity::new(
                next,
                // amount_paid = amount * ((rate / 100) / 100) / 365 * days_to_maturity
                // Note: amount_paid is stored in cents, therefor resulting
                // amount_paid will also be in cents.
                //
                maturity_return,
                wht,
            ));

            next += Duration::days(self.days_to_maturity.into());
        }

        maturities
    }

    pub fn is_current(&self) -> bool {
        self.current
    }
}

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Maturity {
    date: NaiveDate,
    total_return: i64,
    wht: i64,
}

impl Maturity {
    fn new(date: NaiveDate, total_return: i64, wht: i64) -> Self {
        Self {
            date,
            total_return,
            wht,
        }
    }

    pub fn date(&self) -> NaiveDate {
        self.date
    }

    pub fn amount_paid(&self) -> i64 {
        self.total_return - self.wht
    }

    pub fn total_return(&self) -> i64 {
        self.total_return
    }

    pub fn wht(&self) -> i64 {
        self.wht
    }
}
