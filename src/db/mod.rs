/* src/db/mod.rs
 *
 * Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

#[cfg(feature = "app")]
use std::path::PathBuf;

#[cfg(feature = "app")]
use anyhow::{Context, Result};
#[cfg(feature = "app")]
use sqlx::SqlitePool;

mod portfolio;
pub use portfolio::{Portfolio, PortfolioId};

mod account;
pub use account::{Account, AccountId, AccountType};

mod transaction;
pub use transaction::{Maturity, Transaction, TransactionId};

#[cfg(feature = "app")]
pub async fn connect(db_path: PathBuf) -> Result<SqlitePool> {
    let db_pool = SqlitePool::connect(&format!("sqlite://{}?mode=rwc", db_path.display()))
        .await
        .with_context(|| format!("Connecting to database {}", db_path.display()))?;

    sqlx::migrate!()
        .run(&db_pool)
        .await
        .with_context(|| "Running database migrations")?;

    Ok(db_pool)
}
