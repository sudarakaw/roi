/* src/command.rs: Tauti commands exposed to Js/Wasm
 *
 * Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

use std::collections::HashMap;

use sqlx::SqlitePool;
use tauri::{async_runtime, AppHandle, Emitter, Manager, State, Window};

use crate::{
    db::{Account, AccountId, Portfolio, PortfolioId, Transaction},
    DB,
};

#[tauri::command]
pub fn load_main_window(app: AppHandle) {
    let main_window = app.get_webview_window("main").unwrap();
    let loading_window = app.get_webview_window("loading_screen").unwrap();

    main_window.show().unwrap();
    loading_window.close().unwrap();
}

#[tauri::command]
pub fn request_data(window: Window, db_state: State<DB>) {
    let db = db_state.0.clone();

    async_runtime::spawn(async move {
        send_data(&window, &db).await;
    });
}

#[tauri::command]
pub fn change_portfolio(window: Window, db_state: State<DB>, id: PortfolioId) {
    let db = db_state.0.clone();

    async_runtime::spawn(async move {
        let mut portfolio = Portfolio::get(&db, &id).await.unwrap();

        portfolio.selected = true;
        portfolio.save(&db).await.unwrap();

        send_data(&window, &db).await;
    });
}

#[tauri::command]
pub async fn save_portfolio(
    window: Window,
    db_state: State<'_, DB>,
    mut portfolio: Portfolio,
) -> Result<Option<String>, ()> {
    let db = db_state.0.clone();

    let result = portfolio.save(&db).await;

    // NOTE:
    // This command ALWAYS return a successful (`Ok') result. Otherwise WASM
    // (Yew) will throw an exception.
    //
    // Any error message from DB operations will be enclosed in an `Option`
    // - `Ok(None)` indicates a successful save operation.
    // - `Ok(Err(_)) indicates a failed save operation.
    //
    match result {
        Ok(_) => {
            send_data(&window, &db).await;

            Ok(None)
        }
        Err(error) => Ok(Some(error.to_string())),
    }
}

#[tauri::command]
pub async fn save_account(
    window: Window,
    db_state: State<'_, DB>,
    mut account: Account,
) -> Result<Option<String>, ()> {
    let db = db_state.0.clone();

    let result = account.save(&db).await;

    // NOTE:
    // This command ALWAYS return a successful (`Ok') result. Otherwise WASM
    // (Yew) will throw an exception.
    //
    // Any error message from DB operations will be enclosed in an `Option`
    // - `Ok(None)` indicates a successful save operation.
    // - `Ok(Err(_)) indicates a failed save operation.
    //
    match result {
        Ok(_) => {
            send_data(&window, &db).await;

            Ok(None)
        }
        Err(error) => Ok(Some(error.to_string())),
    }
}

#[tauri::command]
pub async fn archive_account(
    window: Window,
    db_state: State<'_, DB>,
    mut account: Account,
) -> Result<Option<String>, ()> {
    let db = db_state.0.clone();

    account.archived = true;

    let result = account.save(&db).await;

    // NOTE:
    // This command ALWAYS return a successful (`Ok') result. Otherwise WASM
    // (Yew) will throw an exception.
    //
    // Any error message from DB operations will be enclosed in an `Option`
    // - `Ok(None)` indicates a successful save operation.
    // - `Ok(Err(_)) indicates a failed save operation.
    //
    match result {
        Ok(_) => {
            send_data(&window, &db).await;

            Ok(None)
        }
        Err(error) => Ok(Some(error.to_string())),
    }
}

#[tauri::command]
pub async fn restore_account(
    window: Window,
    db_state: State<'_, DB>,
    mut account: Account,
) -> Result<Option<String>, ()> {
    let db = db_state.0.clone();

    account.archived = false;

    let result = account.save(&db).await;

    // NOTE:
    // This command ALWAYS return a successful (`Ok') result. Otherwise WASM
    // (Yew) will throw an exception.
    //
    // Any error message from DB operations will be enclosed in an `Option`
    // - `Ok(None)` indicates a successful save operation.
    // - `Ok(Err(_)) indicates a failed save operation.
    //
    match result {
        Ok(_) => {
            send_data(&window, &db).await;

            Ok(None)
        }
        Err(error) => Ok(Some(error.to_string())),
    }
}

#[tauri::command]
pub async fn delete_portfolio(
    window: Window,
    db_state: State<'_, DB>,
    id: PortfolioId,
) -> Result<Option<String>, ()> {
    let db = db_state.0.clone();

    let portfolio = match Portfolio::get(&db, &id).await {
        Ok(portfolio) => portfolio,
        Err(err) => return Ok(Some(err.to_string())),
    };

    let result = portfolio.remove(&db).await;

    // NOTE:
    // This command ALWAYS return a successful (`Ok') result. Otherwise WASM
    // (Yew) will throw an exception.
    //
    // Any error message from DB operations will be enclosed in an `Option`
    // - `Ok(None)` indicates a successful save operation.
    // - `Ok(Err(_)) indicates a failed save operation.
    //
    match result {
        Ok(_) => {
            send_data(&window, &db).await;

            Ok(None)
        }
        Err(error) => Ok(Some(error.to_string())),
    }
}

#[tauri::command]
pub async fn get_transactions(
    db_state: State<'_, DB>,
    accountid: AccountId,
) -> Result<Vec<Transaction>, ()> {
    let db = db_state.0.clone();

    Ok(Transaction::get_all(&db, &accountid).await)
}

#[tauri::command]
pub async fn save_transaction(
    window: Window,
    db_state: State<'_, DB>,
    mut transaction: Transaction,
) -> Result<Option<String>, ()> {
    let db = db_state.0.clone();

    let result = transaction.save(&db).await;

    // NOTE:
    // This command ALWAYS return a successful (`Ok') result. Otherwise WASM
    // (Yew) will throw an exception.
    //
    // Any error message from DB operations will be enclosed in an `Option`
    // - `Ok(None)` indicates a successful save operation.
    // - `Ok(Err(_)) indicates a failed save operation.
    //
    match result {
        Ok(_) => {
            send_data(&window, &db).await;

            Ok(None)
        }
        Err(error) => Ok(Some(error.to_string())),
    }
}

async fn send_data(window: &Window, db: &SqlitePool) {
    let portfolio_list = Portfolio::get_all(db).await;
    let selected_portfolio = portfolio_list
        .clone()
        .into_iter()
        .filter(|p| p.selected)
        .collect::<Vec<Portfolio>>();
    let selected_portfolio = selected_portfolio.first().unwrap();
    let accounts = Account::get_all(db, &selected_portfolio.id).await;
    let mut transactions = HashMap::with_capacity(accounts.len());

    for acc in &accounts {
        let id = acc.get_id();
        let transaction = Transaction::get_current(db, &id).await;

        if let Ok(transaction) = transaction {
            transactions.insert(id, transaction);
        }
    }

    window
        .emit("refresh_data", (portfolio_list, accounts, transactions))
        .unwrap();
}
