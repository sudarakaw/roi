/* src/main.rs
 *
 * Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY; This is free software, and
 * you are welcome to redistribute it and/or modify it under the terms of the
 * BSD 2-clause License. See the LICENSE file for more details.
 *
 */

// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

use anyhow::Result;
use roi::{
    command::*,
    db::{self},
    package, Config, DB,
};
use tauri::{
    generate_handler,
    menu::{Menu, MenuItemBuilder, SubmenuBuilder},
    Manager,
};

#[tokio::main]
async fn main() -> Result<()> {
    let cfg = Config::new()?;
    let db = db::connect(cfg.db_path).await?;

    tauri::Builder::default()
        .setup(move |app| {
            let quit_menu = MenuItemBuilder::new("Quit")
                .id("quit")
                .accelerator("CmdOrControl+Q")
                .build(app)?;

            let menu = SubmenuBuilder::new(app, package::NAME.to_uppercase())
                .item(&quit_menu)
                .build()?;

            let menubar = Menu::with_items(app, &[&menu])?;

            app.set_menu(menubar)?;

            app.on_menu_event(|app, event| match event.id().as_ref() {
                "quit" => app.exit(0),
                _ => {}
            });

            app.get_webview_window("loading_screen")
                .unwrap()
                .hide_menu()
                .unwrap();

            Ok(())
        })
        .manage(DB(db))
        .invoke_handler(generate_handler![
            load_main_window,
            request_data,
            change_portfolio,
            save_portfolio,
            save_account,
            delete_portfolio,
            archive_account,
            restore_account,
            get_transactions,
            save_transaction,
        ])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");

    Ok(())
}
